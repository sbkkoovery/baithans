<?php
include_once("db_functions.php");
class widgets_assign extends db_functions{
     var $tablename = "tbl_hotel_widget";
     var $primaryKey = "hotel_widget_id";
     var $table_fields = array("hotel_widget_id"=>"","hotel_id"=>"","widget_id"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>