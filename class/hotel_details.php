<?php
include_once("db_functions.php");
class hotel_details extends db_functions{
     var $tablename = "hotel_details";
     var $primaryKey = "hd_id";
     var $table_fields = array("hd_id"=>"","h_id"=>"","hd_logo"=>"","hd_place"=>"","hd_welcome_caption"=>"","hd_welcome_descr"=>"","hd_home"=>"","hd_created"=>"","hd_highlights"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>