<?php
include_once("db_functions.php");
class hotel_rooms extends db_functions{
     var $tablename = "hotel_rooms";
     var $primaryKey = "room_id";
     var $table_fields = array("room_id"=>"","h_id"=>"","room_type"=>"","room_cnt"=>"","price"=>"","discount"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>