<?php
include_once("db_functions.php");
class widgets extends db_functions{
     var $tablename = "reason_widget";
     var $primaryKey = "widget_id";
     var $table_fields = array("widget_id"=>"","widget_title"=>"","widget_img"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>