<?php
include_once("db_functions.php");
class bookdetails extends db_functions{
     var $tablename = "book_details";
     var $primaryKey = "bookdetails_id";
     var $table_fields = array('bookdetails_id'=>"",'u_id'=>"",'b_id'=>"",'fname'=>"",'lname'=>"",'email'=>"",'tripwork'=>"",'tripleiss'=>"",'smoke'=>"",'srequest'=>"",'highfloor'=>"",'groundfloor'=>"",'freepark'=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>