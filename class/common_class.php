<?php

class common
{
	public function esc($s){ 
	
	   $s				= htmlentities($s);
	   $s				= str_replace("'","&#39;",$s);
	   $s				= mysql_real_escape_string($s);
	   return $s;
	}
	function adminLogin(){
		$admin	=	false;
		if(isset($_SESSION['master'])){
			$admin	=	true;
		}
		return $admin;
	}
	function adminCheck(){
		if(!isset($_SESSION['master'])){
			header("location:../login.php");
			exit();
		}
	}
	public function html2text($s){
		$s				= html_entity_decode($s);
		$s				= stripslashes($s);
		$s				= str_replace("\r\n","<br/>",$s);
		$s				= str_replace("\\r\\n","<br/>",$s);
		$s				= str_replace("\\n","<br/>",$s);
		$s    			= str_replace("'","&#39;",$s);
		$s				= nl2br($s);
		return $s;
	}
	function html2textarea($s){
		$s				= html_entity_decode($s);
		//$s				= stripslashes($s);
		$s				= str_replace("\r\n","<br/>",$s);
		$s				= str_replace("\\r\\n","<br/>",$s);
		$s				= str_replace("<br/>","\n",$s);
		//$s				= nl2br($s);
		return $s;
	}
	function displayEditor($s){
		$s				= stripslashes($s);
		$s				= str_replace("rn","",$s);
		return $s;
	}
	function addMsg($message,$type){
		$messageArray	=	array("msg"=>$message,"type"=>$type);
		$_SESSION["message"]=$messageArray;
	}
	function displayMsg(){
		if(isset($_SESSION['message'])){
			$message	=	$_SESSION['message']["msg"];
			$type		=	$_SESSION['message']["type"];
			$selectClass=	($type==1)?"success":"danger";
			$messageBox	=	 '<div class="alert alertClose alert-'.$selectClass.' alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alertClose" aria-label="Close"><span aria-hidden="true">&times;</span></button> '.$message.'</div><script>$( ".alertClose" ).delay( 4000 ).slideUp( 400 );</script>';
							unset($_SESSION['message']);
							if(isset($_SESSION['sect'])){
								unset($_SESSION['sect']);
							}
							return $messageBox;
		}
	}
	function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		   $ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	function randStrGen($len){
		$result = "";
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789";
		$charArray = str_split($chars);
		for($i = 0; $i < $len; $i++){
			$randItem = array_rand($charArray);
			$result .= "".$charArray[$randItem];
		}
		return $result;
	}
	function randStrGenSpecial($len){
		$result = "";
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789@$*_";
		$charArray = str_split($chars);
		for($i = 0; $i < $len; $i++){
			$randItem = array_rand($charArray);
			$result .= "".$charArray[$randItem];
		}
		return $result;
	}
	function addIMG($file,$folder,$name,$width,$height,$thump){
		$ret="";
		if(isset($file)){
			$ext=explode(".",$file['name']);
			$ext=(count($ext)!=0)?strtolower($ext[count($ext)-1]):"";
			$path="$folder$name.$ext";
			$name="$name.$ext";
			if(copy($file['tmp_name'],$path)){
				$proc=false;
				switch($ext){
					case "jpg": $proc=true; $im=imagecreatefromjpeg($path); break;
					case "jpeg": $proc=true; $im=imagecreatefromjpeg($path); break;
					case "gif": $proc=true; $im=imagecreatefromgif($path); break;
					case "png": $proc=true; $im=imagecreatefrompng($path); break;
				}
				if($proc){
					$ow=imagesx($im);
					$oh=imagesy($im);
					$bow=$ow;
					$boh=$oh;
					$posX=0;
					$posY=0;
					if($ow>$width||$oh>$height){
						if($thump){ 
							$cmp=1; 
							if($oh>$ow){ $cmp = $ow/$width; }
							if($ow>$oh){ $cmp = $oh/$height; }
							if($ow==$oh){ $cmp = $oh/$height; }
							$ow=round($ow/$cmp); 
							$oh=round($oh/$cmp);
							if($ow<$width){
								$cmp = $ow/$width;
								$ow=round($ow/$cmp); 
								$oh=round($oh/$cmp);
							}
							if($oh<$height){
								$cmp = $oh/$height;
								$ow=round($ow/$cmp); 
								$oh=round($oh/$cmp);
							}
						}else{
							$cmp=1; 
							if($ow>$oh){ $cmp = $ow/$width; }
							if($ow<$oh){ $cmp = $oh/$height; }
							if($ow==$oh){ $cmp = $oh/$height; }
							$ow=round($ow/$cmp); 
							$oh=round($oh/$cmp);
						}
					}
					if($thump){ 
						$posX=round(($width-$ow)/2); 
						$posY=round(($height-$oh)/2); 
					}
					$bow1=$ow; 
					$boh1=$oh; 
					if($thump){ 
						$ow=$width; 
						$oh=$height; 
					}
					$newImg = imagecreatetruecolor($ow,$oh);
					if($ext=="png"){
						imagealphablending($newImg, false);
						imagesavealpha($newImg,true);
						$transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
						imagefilledrectangle($newImg, 0, 0, $ow, $oh, $transparent);
					}
					imagecopyresampled($newImg, $im, $posX, $posY, 0, 0 , $bow1, $boh1, $bow, $boh);
					if($ext=="png"){ 
						imagepng($newImg,$path,9); 
					}else if($ext=="jpg"){ 
						imagejpeg($newImg,$path,90); 
					}else if($ext=="gif"){ 
						imagegif($newImg,$path); 
					}
					$ret= $name;
				}
			}
		}
		return $ret;
	}
	function getAlias($title){
		$alias	=	strtolower($title);		
		$alias	=	preg_replace("/[^a-z0-9]+/", "-", $alias);
		$alias	=	rtrim($alias, '-');
		$alias	=	ltrim($alias, '-');
		return $alias;
	}
	function getAge($birthDate){
		$age	=	floor((time()-strtotime($birthDate))/31556926);
		return $age;
	}
	function limitWords($pageContent,$limit){
		$resultWord		=	$this->html2text($pageContent);
		if(strlen($resultWord)>$limit){
			$resultWord	=	mb_substr($resultWord,0,$limit,'UTF-8')."...";
		}
		return $resultWord;
	}
	function smallWords($str,$count){
		if (strlen($str) > $count){
			$str = wordwrap($str, $count);
			$str = substr($str, 0, strpos($str, "\n"));
		}
		return $str;
	}

	function round_to_nearest_half($number) {
		return round($number * 2) / 2;
	}
	function getThumb($ar)
	{
		$arrExpl				=	explode("/",$ar);
		$thumbPath			  =	$arrExpl[0].'/thumb/'.$arrExpl[1];	
		return $thumbPath;			 				 
	}
	function currencies(){
			$arry			   =	array('$'=>"USD",'EURO'=>"EURO",'AED'=>"AED");
			return $arry;
	}

}
?>
