<?php
include_once("db_functions.php");
class hotels extends db_functions{
     var $tablename = "hotels";
     var $primaryKey = "h_id";
     var $table_fields = array('h_id'=>"",'h_name'=>"",'h_alias'=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>