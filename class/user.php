<?php
include_once("db_functions.php");
class user extends db_functions{
     var $tablename = "user";
     var $primaryKey = "id";
     var $table_fields = array('id'=>"",'name'=>"",'email'=>"",'password'=>"",'phone'=>"",'location'=>"",'hash'=>"",'active'=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>