<?php
include_once("db_functions.php");
class bookings extends db_functions{
     var $tablename = "bookings";
     var $primaryKey = "b_id";
     var $table_fields = array("b_id"=>"","h_id"=>"","room_id"=>"","user_id"=>"","checkin"=>"","checkout"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>