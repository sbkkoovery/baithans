<?php
include_once("db_functions.php");
class hotel_sliders extends db_functions{
     var $tablename = "hotel_sliders";
     var $primaryKey = "hs_id";
     var $table_fields = array("hs_id"=>"","h_id"=>"","hs_img"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>