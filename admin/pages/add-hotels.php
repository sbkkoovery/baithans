<?php
include_once("../class/hotels.php");
include_once("../class/common_class.php");
$objHotels			  	   =	new hotels();
$objCommon		 		   =	new common();
$nId			  			 =	$objCommon->esc($_GET['nId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_GET['search']);
if($nId){
		$getRowDetails	   =	$objHotels->getRow("h_id=".$nId);
}
if($dId){
		$objHotels->delete("h_id=".$dId);
		$objCommon->addMsg("Selected item has been deleted successfully.",1);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
}
$sql						 .= "SELECT * FROM hotels WHERE 1 ";
if($search){
	$sql					.= " AND (h_name LIKE '%".$search."%' OR h_id LIKE '%".$search."%')";	
}
$sql						 .= " ORDER by h_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objHotels->listQuery($paginationQuery);
?>
<div class="page-heading">
	<h3>Hotels</h3>
	<ul class="breadcrumb">
		<li><a href="#">Hotels</a></li>
		<li class="active"> Add New</li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            <div class="col-lg-4">
                <section class="panel">
                    <header class="panel-heading">Add Hotels</header>
                    <div class="panel-body">
                        <form role="form" id="add_main_category" method="post" action="access/add_hotels.php">
                            <div class="form-group">
                                <label>Hotel Name</label>
                                <input type="text" name="h_name" id="h_name" class="form-control" id="exampleInputEmail1" value="<?php echo ($getRowDetails['h_name'])?$objCommon->html2text($getRowDetails['h_name']):''?>" placeholder="Enter Title" required >
                            </div>
                            <input type="hidden" name="editId" value="<?php echo $nId?>" />
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </section>
            </div>
            <div class="col-lg-8">
                <section class="panel">
                    <header class="panel-heading">Hotel List</header>
                    <div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-6 col-md-offset-6 ">
                                <form class="form-search pull-right" method="get" action="">
                                	<input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                                    <input type="hidden" name="page" value="add-hotels" />
                               		 <button class="btn btn-primary search_submit" type="submit">Submit</button>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="25%">Name</th>
										<th width="25%">Alias</th>
                                        <th width="10%">ID</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($contentList)>0){
                                    $i=1;
                                    foreach($contentList as $list){?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $objCommon->html2text($list['h_name']); ?></td>
										<td><?php echo $objCommon->html2text($list['h_alias']); ?></td>
                                        <td><?php echo $list['h_id']; ?></td>
                                        <td>
											<a href="?page=add-hotels&nId=<?php echo $list['h_id']?> " class="actionLink " title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;
											<!--<a href="?page=add-hotels&dId=<?php echo $list['h_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>&nbsp;-->
											<a href="?page=add-rooms&hid=<?php echo $list['h_id']?>" title="Add Rooms"><i class="fa fa-plus-circle"></i></a>&nbsp;
											<a href="?page=add-hotel-details&hid=<?php echo $list['h_id']?>" title="Add Details"><i class="fa fa-file-text-o"></i></a>&nbsp;
                                            <a href="?page=list-imgs&hid=<?php echo $list['h_id']?>" title="List Images"><i class="fa fa-file-text-o"></i></a>&nbsp;
                                            <a href="?page=add-hotels&dId=<?php echo $list['h_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>&nbsp;
											
                                        </td>                	
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="4">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
                    </div>
                </section>

            </div>
        </div>