<?php
include_once("../class/hotel_sliders.php");
include_once("../class/hotel_details.php");
include_once("../class/hotels.php");
$objHotels		=	new hotels();
include_once("../class/common_class.php");
$objImgs			  	   =	new hotel_sliders();
$objHoteldetails		=	new hotel_details();
$objCommon		 		   =	new common();
$imgId			  			 =	$objCommon->esc($_GET['hid']);
$delimgId			  		 =	$objCommon->esc($_GET['delimgId']);
$dellogoId			  		 =	$objCommon->esc($_GET['dellgId']);
$delhmId			  		 =	$objCommon->esc($_GET['delhomeId']);
	//$getRowDetails	   =	$objImgs->getRow("hs_id=".$imgId);

if($delimgId){
		$objImgs->delete("hs_id=".$delimgId);
		$objCommon->addMsg("Image has been deleted successfully.",1);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
}
if($imgId){
$logo                     = "SELECT hd_logo FROM hotel_details WHERE h_id=".$imgId."";
$logoList				 = $objHoteldetails->listQuery($logo);

$hmImg                     = "SELECT hd_home FROM hotel_details WHERE h_id=".$imgId."";
$hmList				 = $objHoteldetails->listQuery($hmImg);
	
$sql						 .= "SELECT * FROM hotel_sliders WHERE h_id=".$imgId." ORDER by hs_id DESC ";	
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objImgs->listQuery($paginationQuery);
}
else
{
$logo                     = "SELECT hd_logo FROM hotel_details";
$logoList				 = $objHoteldetails->listQuery($logo);

$hmImg                     = "SELECT hd_home FROM hotel_details";
$hmList				 = $objHoteldetails->listQuery($hmImg);	

$sql						 .= "SELECT * FROM hotel_sliders ORDER by hs_id DESC ";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objImgs->listQuery($paginationQuery);
}
?>
<div class="page-heading">
	<h3>List Images</h3>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
                      
                                      
            <div class="col-lg-8">
                <section class="panel">
                    <header class="panel-heading">Logo </header>
                        <div class="table-responsive">
                        <form name="assign_form" id="assign_form" action="pages/list-images.php" method="post" onsubmit="return check_selection()">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                    	<th width="70%">Logo</th>
                                        <!--<th>Action</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($logoList)>0){
                                    $i=1;
									$count_no = 0;
                                    foreach($logoList as $lglist){$count_no++;?>
                                    <tr>
                                       
                                        <td><?php echo $i; ?></td>
                                       	<td><a href="../uploads/hotels/logo/<?php echo $objCommon->html2text($lglist['hd_logo']); ?>" class="html5lightbox"><img src="../uploads/hotels/logo/<?php echo $objCommon->html2text($lglist['hd_logo']); ?>" width="100px" height="100px" /></a></td>
                                       <!-- <td>
											<a href="index.php?page=list-imgs&dellgId=<?php echo $list['hs_id']; ?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>&nbsp;
											
                                        </td>  -->              	
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="5">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                                                      
                           </form>
                       </div>
                </section>
                 <section class="panel">
                    <header class="panel-heading">Home Image </header>
                        <div class="table-responsive">
                        <form name="assign_form" id="assign_form" action="pages/list-images.php" method="post" onsubmit="return check_selection()">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                    	<th width="70%">Home Image</th>
                                      <!--  <th>Action</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($hmList)>0){
                                    $i=1;
									$count_no = 0;
                                    foreach($hmList as $homlist){$count_no++;?>
                                    <tr>
                                       
                                        <td><?php echo $i; ?></td>
                                       	<td><a href="../uploads/hotels/home_image/<?php echo $objCommon->html2text($homlist['hd_home']); ?>" class="html5lightbox"><img src="../uploads/hotels/home_image/<?php echo $objCommon->html2text($homlist['hd_home']); ?>" width="100px" height="100px" /></a></td>
                                       <!-- <td>
											<a href="index.php?page=list-imgs&delimgId=<?php echo $list['hs_id']; ?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>&nbsp;
											
                                        </td>-->                	
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="5">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                                                      
                           </form>
                       </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">Sliders </header>
                        <div class="table-responsive">
                        <form name="assign_form" id="assign_form" action="pages/list-images.php" method="post" onsubmit="return check_selection()">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                    	<th width="75%">Image</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($contentList)>0){
                                    $i=1;
									$count_no = 0;
                                    foreach($contentList as $list){$count_no++;?>
                                    <tr>
                                       
                                        <td><?php echo $i; ?></td>
                                       	<td><a href="../uploads/hotels/slider/<?php echo $objCommon->html2text($list['hs_img']); ?>" class="html5lightbox"><img src="../uploads/hotels/slider/<?php echo $objCommon->html2text($list['hs_img']); ?>" width="100px" height="100px" /></a></td>
                                        <td>
											<a href="index.php?page=list-imgs&delimgId=<?php echo $list['hs_id']; ?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>&nbsp;
											
                                        </td>                	
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="5">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                                                      
                            </form>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
                    </div>
                </section>

            </div>
            
            
            
        </div>
        
        <script language="javascript">

function check_selection()

{
	var hotel_check = document.getElementById('hotel_id').value;
	var wid_variable = 0;
	
	 <?php 
                                    if(count($contentList)>0){
									$count_no = 0;
                                    foreach($contentList as $list){$count_no++;
	?>
	if(document.getElementById('list'+<?php echo $count_no; ?>).checked == true)
	{
		wid_variable++;
	}
	<?php
									}}
									else
									{
	?>
	var wid_variable = 0;
	<?php
									}
	?>
								
	if(hotel_check == '' || wid_variable==0)
	{
		alert('select hotel and widget to assign');
		return false;
		

	}
	
	

}


</script>