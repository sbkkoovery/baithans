<?php
include_once("../class/pages.php");
$objPages		=	new pages();
include_once("../class/common_class.php");
include("includes/ckeditor/ckeditor.php");
$objPages			  	   =	new pages();
$objCommon		 		   =	new common();
$wId			  			 =	$objCommon->esc($_GET['wId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_POST['search']);
if($wId){
		$getRowDetails	   =	$objPages->getRow("page_id=".$wId);
}
if($dId){
		$objPages->delete("page_id=".$dId);
		$objCommon->addMsg("Selected item has been deleted successfully.",1);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
}
$sql						 .= "SELECT * FROM pages WHERE 1 ";
if($search){
	$sql					.= " AND (page_title LIKE '%".$search."%' OR page_id LIKE '%".$search."%')";	
}
$sql						 .= " ORDER by page_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objPages->listQuery($paginationQuery);



?>
<div class="page-heading">
	<h3>Pages</h3>
	<ul class="breadcrumb">
		<li><a href="#">Pages</a></li>
		<li class="active"> Add Pages</li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">Add Pages</header>
                    <div class="panel-body">
                        <form role="form" id="add_main_category" method="post" action="access/add-pages.php" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Page Title</label>
                                <input type="text" name="page_title" id="page_title" class="form-control" value="<?php echo ($getRowDetails['page_title'])?$objCommon->html2text($getRowDetails['page_title']):''?>" placeholder="Enter Title" required >
                            </div>
                            
                            <div class="form-group">
                                <label>Alias</label>
                                <input type="text" name="page_alias" id="page_alias" class="form-control" value="<?php echo ($getRowDetails['page_alias'])?$objCommon->html2text($getRowDetails['page_alias']):''?>" placeholder="Enter Alias" required >
                            </div>
                            
                            <div class="form-group">
                                <label>Page contents</label>
                                <!--<textarea name="page_des" id="page_des" class="TextBox300"><?php echo ($getRowDetails['page_alias'])?$objCommon->html2text($getRowDetails['page_des']):''?></textarea>-->
                                <textarea class="form-control ckeditor" name="page_des" id="page_des" rows="6"><?php echo ($getRowDetails['page_des'])?$objCommon->html2text($getRowDetails['page_des']):''?></textarea>
              <script type="text/javascript">
	   CKEDITOR.replace( 'page_des')
      </script>
                                
                            </div>
                            <input type="hidden" name="editId" value="<?php echo $wId?>" />
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </section>
            </div>
            
            
        </div>
        
       