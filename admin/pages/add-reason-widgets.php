<?php
include_once("../class/widgets.php");
include_once("../class/hotels.php");
include_once("../class/widgets_assign.php");

$objHotels		=	new hotels();
include_once("../class/common_class.php");
$objWidgets			  	   =	new widgets();
$objCommon		 		   =	new common();
$objWidgets_assign		 		   =	new widgets_assign();
$wId			  			 =	$objCommon->esc($_GET['wId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_POST['search']);
if($wId){
		$getRowDetails	   =	$objWidgets->getRow("widget_id=".$wId);
}
if($dId){
		$objWidgets->delete("widget_id=".$dId);
		$objCommon->addMsg("Selected item has been deleted successfully.",1);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
}
$sql						 .= "SELECT * FROM reason_widget WHERE 1 ";
if($search){
	$sql					.= " AND (widget_title LIKE '%".$search."%' OR widget_id LIKE '%".$search."%')";	
}
$sql						 .= " ORDER by widget_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objWidgets->listQuery($paginationQuery);

$getHotel					 =	$objHotels->listQuery("select * from hotels");


?>
<div class="page-heading">
	<h3>Reason Widgets</h3>
	<ul class="breadcrumb">
		<li><a href="#">Reason Widgets</a></li>
		<li class="active"> Add New</li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            <div class="col-lg-4">
                <section class="panel">
                    <header class="panel-heading">Add Reason Widget</header>
                    <div class="panel-body">
                        <form role="form" id="add_main_category" method="post" action="access/add-reason-widgets.php" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Widget Name</label>
                                <input type="text" name="widget_title" id="widget_title" class="form-control" value="<?php echo ($getRowDetails['widget_title'])?$objCommon->html2text($getRowDetails['widget_title']):''?>" placeholder="Enter Title" required>
                            </div>
                            
                            <div class="form-group">
                                <label>Image</label>
                                <input type="file" name="widget_img" id="widget_img" class="form-control" value="<?php echo ($getRowDetails['widget_title'])?$objCommon->html2text($getRowDetails['widget_title']):''?>" placeholder="Enter Title" <?php if($wId == '') { ?>required<?php } ?> >
                            </div>
                            <input type="hidden" name="editId" value="<?php echo $wId?>" />
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </section>
            </div>
            
            
            <div class="col-lg-8">
                <section class="panel">
                    <header class="panel-heading">Widget List</header>
                    <div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-6 col-md-offset-6 ">
                                <form class="form-search pull-right" method="post" action="">
                                	<input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                                    <input type="hidden" name="page" value="add-hotels" />
                               		 <button class="btn btn-primary search_submit" type="submit">Submit</button>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                        <form name="assign_form" id="assign_form" action="access/assign-widgets.php" method="post" onsubmit="return check_selection()">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">&nbsp;</th>
                                        <th width="5%">No</th>
                                        <th width="25%">Widget</th>
										<th width="25%">Image</th>
                                        <th width="10%">ID</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($contentList)>0){
                                    $i=1;
									$count_no = 0;
                                    foreach($contentList as $list){$count_no++;
									
									if($_GET["selected"] != '')
									{
									$getAssignedW = $objWidgets_assign->listQuery("select * from tbl_hotel_widget WHERE hotel_id=".$_GET["selected"]." AND widget_id=".$list['widget_id']);
									}
									?>
                                    <tr>
                                        <td><input name="list[]" id="list<?php echo $count_no; ?>" class="chkbx" value="<?php echo $objCommon->html2text($list['widget_id']); ?>"type="checkbox" <?php if($_GET["selected"] != ''){ if(count($getAssignedW)>0) { ?> checked="checked"<?php } } ?> /></td>
                                      <td><?php echo $i; ?></td>
                                        <td><?php echo $objCommon->html2text($list['widget_title']); ?></td>
										<td><img src="../widgets/<?php echo $objCommon->html2text($list['widget_img']); ?>" width="
                                        150px" height="150px" /></td>
                                        <td><?php echo $list['widget_id']; ?></td>
                                        <td>
											<a href="index.php?page=add-reason-widgets&wId=<?php echo $list['widget_id']; ?>" class="actionLink " title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;
											<a href="index.php?page=add-reason-widgets&dId=<?php echo $list['widget_id']; ?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>&nbsp;
											
                                        </td>                	
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="5">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                            
                            <?php if($_GET["selected"] != '') {?>
                            
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="40%"><div class="form-group">
				<select name="hotel_id" id="hotel_id" class="form-control">
					<option value="">Select</option>
					<?php
					foreach($getHotel as $allHotel){
						?>
						<option value="<?php echo $allHotel['h_id']?>" <?php if($_GET["selected"] != ''){ if($_GET["selected"] == $allHotel['h_id']) {?> selected="selected" <?php } } ?>><?php echo $objCommon->html2text($allHotel['h_name'])?></option>
						<?php
					}
					?>
				</select>
				 </div></th>
                                        <th width="60%"><input type="submit" name="assign_submit" id="assign_submit" value="Assign" /></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($contentList)>0){
                                    $i=1;
                                    foreach($contentList as $list){?>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="2">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                            <?php 
							
							}
							
							?>
                            </form>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
                    </div>
                </section>

            </div>
            
            
            
        </div>
        
        <script language="javascript">

function check_selection()

{
	var hotel_check = document.getElementById('hotel_id').value;
	var wid_variable = 0;
	
	 <?php 
                                    if(count($contentList)>0){
									$count_no = 0;
                                    foreach($contentList as $list){$count_no++;
	?>
	if(document.getElementById('list'+<?php echo $count_no; ?>).checked == true)
	{
		wid_variable++;
	}
	<?php
									}}
									else
									{
	?>
	var wid_variable = 0;
	<?php
									}
	?>
								
	if(hotel_check == '' || wid_variable==0)
	{
		alert('select hotel and widget to assign');
		return false;
		

	}
	
	

}


</script>