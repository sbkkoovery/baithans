<?php
include_once("../class/widgets.php");
include_once("../class/hotels.php");
include_once("../class/widgets_assign.php");
$objHotels		=	new hotels();
include_once("../class/common_class.php");
$objWidgets			  	   =	new widgets();
$objCommon		 		   =	new common();
$objWidgets_assign		 		   =	new widgets_assign();
$wId			  			 =	$objCommon->esc($_GET['wId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_POST['search']);
if($wId){
		$getRowDetails	   =	$objWidgets->getRow("h_id=".$wId);
}
if($dId){
		$objWidgets->delete("h_id=".$dId);
		$objCommon->addMsg("Selected item has been deleted successfully.",1);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
}
$sql						 .= "SELECT * FROM hotels WHERE 1 ";
if($search){
	$sql					.= " AND (h_name LIKE '%".$search."%' OR h_id LIKE '%".$search."%')";	
}
$sql						 .= " ORDER by h_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objWidgets->listQuery($paginationQuery);

$getHotel					 =	$objHotels->listQuery("select * from hotels");

?>
<div class="page-heading">
	<h3>Assign Widgets</h3>
	<ul class="breadcrumb">
		<li><a href="#">Assign Widgets</a></li>
		<li class="active"> Assign Widgets</li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            
            
            
            <div class="col-lg-10">
                <section class="panel">
                    <header class="panel-heading">Assign Widgets</header>
                    <div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-6 col-md-offset-6 ">
                                <form class="form-search pull-right" method="post" action="">
                                	<input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                                    <input type="hidden" name="page" value="add-hotels" />
                               		 <button class="btn btn-primary search_submit" type="submit">Submit</button>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                        <form name="assign_form" id="assign_form" action="access/assign-widgets.php" method="post" onsubmit="return check_selection()">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">&nbsp;</th>
                                        <th width="5%">No</th>
                                        <th width="25%">Hotel</th>
										<th width="21%">Alias</th>
										<th width="17%">Widgets</th>
                                        <th width="13%">ID</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($contentList)>0){
                                    $i=1;
									$count_no = 0;
                                    foreach($contentList as $list){$count_no++;?>
                                    <tr>
                                        <td><input name="list[]" id="list<?php echo $count_no; ?>" class="chkbx" value="<?php echo $objCommon->html2text($list['h_id']); ?>"type="checkbox" /></td>
                                      <td><?php echo $i; ?></td>
                                        <td><a href="index.php?page=add-reason-widgets&selected=<?php echo $objCommon->html2text($list['h_id']); ?>" class="actionLink " title="Assign"><?php echo $objCommon->html2text($list['h_name']); ?></a></td>
										<td><a href="index.php?page=add-reason-widgets&selected=<?php echo $objCommon->html2text($list['h_id']); ?>" class="actionLink " title="Assign"><?php echo $objCommon->html2text($list['h_alias']); ?></a></td>
										<td><?php $getAssignedW = $objWidgets_assign->listQuery("select * from tbl_hotel_widget WHERE hotel_id=".$objCommon->html2text($list['h_id']));
										if(count($getAssignedW)>0)
										{
											foreach($getAssignedW as $assigned){
											$getOurwidgets = $objWidgets->listQuery("select * from reason_widget WHERE widget_id=".$objCommon->html2text($assigned['widget_id']));
											
											if(count($getOurwidgets)>0)
										{
											foreach($getOurwidgets as $ou_widgets){
												echo $objCommon->html2text($ou_widgets['widget_title']).'<br>';
											}
										}
											
											}
										}
										else
										{
											echo 'No assigned widgets';
										}
										 ?></td>
                                        <td><?php echo $list['h_id']; ?></td>
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="6">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                            
                            
                            </form>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
                    </div>
                </section>

            </div>
            
            
            
        </div>
        
        