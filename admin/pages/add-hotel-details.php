<?php
include_once("../class/hotels.php");
include_once("../class/common_class.php");
include("includes/ckeditor/ckeditor.php");
$objHotels			  	   =	new hotels();
$objCommon		 		   =	new common();
$hid			  			 =	$objCommon->esc($_GET['hid']);
if($hid){
		$getRowDetails	   =	$objHotels->getRowSql("SELECT hotel.h_name,det.*
												FROM hotels AS hotel 
												LEFT JOIN hotel_details AS det ON hotel.h_id = det.h_id 
												LEFT JOIN  (SELECT count(hs_id) AS countSlider,h_id FROM hotel_sliders GROUP BY h_id ) AS slider ON hotel.h_id = slider.h_id
												WHERE hotel.h_id=".$hid);
}
/*echo "SELECT hotel.h_name,det.*,slider.countSlider 
												FROM hotels AS hotel 
												LEFT JOIN hotel_details AS det ON hotel.h_id = det.h_id 
												LEFT JOIN  (SELECT count(hs_id) AS countSlider,h_id FROM hotel_sliders GROUP BY h_id ) AS slider ON hotel.h_id = slider.h_id
												WHERE hotel.h_id=".$hid;*/
?>
<div class="page-heading">
	<h3>Rooms</h3>
	<ul class="breadcrumb"><li><a href="#">Hotels</a></li><li>Add Details</li><li class="active"><?php echo $objCommon->html2text($getRowDetails['h_name'])?></li></ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            <div class="col-lg-10">
                <section class="panel">
                    <header class="panel-heading">Add Details</header>
                    <div class="panel-body">
                        <form role="form" id="add_main_category" method="post" action="access/add_hotel_details.php" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Hotel Name : <b><?php echo $objCommon->html2text($getRowDetails['h_name'])?></b></label>
                            </div>
							<div class="form-group ovr">
								<div class="col-md-6">
									<label>Place</label>
									<input type="text" name="hd_place" id="hd_place" class="form-control"  value="<?php echo ($getRowDetails['hd_place'])?$objCommon->html2text($getRowDetails['hd_place']):''?>" placeholder="Enter Place" >
								</div>
                                <div class="col-md-6">
									<label>Welcome Text</label>
									<input type="text" name="hd_welcome_caption" id="hd_welcome_caption" class="form-control"  value="<?php echo ($getRowDetails['hd_welcome_caption'])?$objCommon->html2text($getRowDetails['hd_welcome_caption']):''?>" placeholder="Enter Title" >
								</div>
                            </div>
							<div class="form-group">
                                <label>Welcome Description</label>
								<textarea name="hd_welcome_descr" class="form-control" id="hd_welcome_descr" placeholder="Enter Welcome Description"><?php echo ($getRowDetails['hd_welcome_descr'])?$objCommon->html2text($getRowDetails['hd_welcome_descr']):''?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Highlights</label>
                                <textarea class="form-control ckeditor" name="hd_highlights" id="hd_highlights" rows="6"><?php echo ($getRowDetails['hd_highlights'])?$objCommon->html2text($getRowDetails['hd_highlights']):''?></textarea>
								<!--<textarea name="hd_highlights" class="form-control" id="hd_highlights" placeholder="Add highlights"><?php echo ($getRowDetails['hd_highlights'])?$objCommon->html2text($getRowDetails['hd_highlights']):''?></textarea>-->
                            </div>
							<div class="form-group ovr">
								<div class="col-md-4">
									<label>Logo</label>
									<input type="file" name="hd_logo" class="form-control"  >
								</div>
								<div class="col-md-4">
									<label>Background Images</label>
									<input type="file" name="hd_bg[]"  class="form-control" multiple="multiple"  >
								</div>
                                <div class="col-md-4">
									<label>Home Image</label>
									<input type="file" name="hd_home"  class="form-control" >
								</div>
                            </div>
							
							<div class="form-group">
							<input type="hidden" name="h_id" value="<?php echo $hid?>" />
                            <button type="submit" class="btn btn-primary">Submit</button>
							</div>
                        </form>

                    </div>
                </section>
            </div>
        </div>
<script type="text/javascript">
	   CKEDITOR.replace( 'hd_highlights')
      </script>