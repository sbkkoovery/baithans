<?php
include_once("../class/hotels.php");
include_once("../class/hotel_rooms.php");
include_once("../class/common_class.php");
$obj_hotel_rooms			   =	new hotel_rooms();
$objHotels			  	   =	new hotels();
$objCommon		 		   =	new common();
$hid			  			 =	$objCommon->esc($_GET['hid']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
if($dId){
		$obj_hotel_rooms->delete("room_id=".$dId);
		$objCommon->addMsg("Selected room has been deleted successfully.",1);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
}
if($hid){
		$getRowDetails	   =	$objHotels->getRow("h_id=".$hid);
}
$sql						 .= "SELECT * FROM hotel_rooms WHERE h_id = ".$hid." ORDER by room_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$roomList				 =	$obj_hotel_rooms->listQuery($paginationQuery);


?>
<div class="page-heading">
	<h3>Rooms</h3>
	<ul class="breadcrumb"><li><a href="#">Hotels</a></li><li>Add Rooms</li><li class="active"><?php echo $objCommon->html2text($getRowDetails['h_name'])?></li></ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">

<div class="col-lg-8">
                <section class="panel">
                    <header class="panel-heading">Rooms List</header>
                    <div class="panel-body">
                    	   <div class="table-responsive">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="25%">Name</th>
										<th width="15%">Rooms</th>
                                        <th width="20%">Price</th>
                                        <th width="20%">Discount(%)</th>
                                        <th width="5%">Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($roomList)>0){
                                    $i=1;
                                    foreach($roomList as $list){?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $objCommon->html2text($list['room_type']); ?></td>
										<td><?php echo $objCommon->html2text($list['room_cnt']); ?></td>
                                        <td><?php echo $objCommon->html2text($list['price']); ?></td>
                                        <td><?php echo $objCommon->html2text($list['discount']); ?></td>
                                        <td>
											<a href="?page=add-rooms&dId=<?php echo $list['room_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>&nbsp;
										</td>                	
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="4">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
                    </div>
                </section>

            </div>
            
            <div class="col-lg-10">
                <section class="panel">
                    <header class="panel-heading">Add Rooms</header>
                    <div class="panel-body">
                        <form role="form" id="add_main_category" method="post" action="access/add_hotel_rooms.php">
                            <!--<div class="form-group">
                                <label>Hotel Name : <b><?php echo $objCommon->html2text($getRowDetails['h_name'])?></b></label>
                            </div>-->
							<div class="form-group">
                                <label>Room Type</label>
                            </div>
							<div id="adddiv" class="form-group ovr">
								 <div class="col-lg-5">
								 	<input type="text" name="room_title[]" id="room_title[]" class="form-control"  placeholder="Enter Title Eg:One-Bedroom Apartment" required >
								 </div>
								 <div class="col-lg-5">
								 	<select  name="room_count[]" id="room_count[]" class="form-control" >
									<option value="0">Select Room Count</option>
									<?php
									for($i=1;$i<=10;$i++){
									?>
									<option value="<?php echo $i;?>"><?php echo $i;?></option>
									<?php
									}
									?>
								</select>
								 </div>
                                  <!-- <div class="col-lg-3">
                                 <select  name="maxpeople[]" id="maxpeople[]" class="form-control" >
                                 <option value="">Maximum People</option>
                                 <option value="2 Adults 1 Child">2 Adults 1 Child</option>
                                 <option value="3 Adults 1 Child">3 Adults 1 Child</option>
                                 <option value="4 Adults 1 Child">4 Adults 1 Child</option>
                                 </select>
                                 </div>
								-->
								
								 <div class="col-lg-12">
									 <div class="row" style="margin-top:5px;">
										 <div class="col-lg-5">
                                         <input type="text" name="price[]" id="price[]" class="form-control"  placeholder="Price" required ></div>
										 <div class="col-lg-5">
                                         <input type="text" name="discount[]" id="discount[]" class="form-control"  placeholder="Discount%" required ></div>
                                         <div class="col-lg-2" style="margin-top:10px;"><a href="javascript:;" class="alink" id="addMoreRooms" onclick="addMoreRooms();">Add More Rooms</a></div>
									 </div>
								</div>
							</div>
							
							
							
							<div class="loadAddMoreRooms"></div>
							<input type="hidden" name="h_id" value="<?php echo $hid?>" />
							
							
							<div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
							</div>
                        </form>

                    </div>
                </section>
            </div>
        </div>
<script>
 var divcount=0;
function addMoreRooms(){
	divcount++;
	var divd = "deld"+divcount;
	//alert(divd);
	var appendStr		=	'<div id="deld'+divcount+'" class="form-group ovr"><div class="col-lg-5"><input type="text" name="room_title[]" id="room_title[]" class="form-control"  placeholder="Enter Title Eg:One-Bedroom Apartment" required  ></div><div class="col-lg-5"><select  name="room_count[]" id="room_count[]" class="form-control"><option value="0">Select Room Count</option>';
	for(var i=1;i<=100;i++){
		appendStr		+=	'<option value="'+i+'">'+i+'</option>';
	}
	appendStr			+=	'</select></div>';
	appendStr			+=	'<div class="col-lg-12"><div class="row" style="margin-top:5px;"><div class="col-lg-5"><input type="text" name="price[]" id="price[]" class="form-control"  placeholder="Price" required ></div><div class="col-lg-5"><input type="text" name="discount[]" id="discount[]" class="form-control"  placeholder="Discount%" required ></div>';
	
	appendStr            +=    '<div class="col-lg-2"><a id='+divcount+' href="javascript:;" class="alink delete" onclick="deldiv(this.id);">Delete</a></div></div></div></div>';
	
	$(".loadAddMoreRooms").append(appendStr);
}

function deldiv(id){
	var did = "deld"+id;
	//alert(did);	
	document.getElementById(did).style.display='none';
}
</script>