<?php
include_once("../class/pages.php");
$objPages		=	new pages();
include_once("../class/common_class.php");
include("includes/ckeditor/ckeditor.php");
$objPages			  	   =	new pages();
$objCommon		 		   =	new common();
$wId			  			 =	$objCommon->esc($_GET['wId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_POST['search']);
if($wId){
		$getRowDetails	   =	$objPages->getRow("page_id=".$wId);
}
if($dId){
		$objPages->delete("page_id=".$dId);
		$objCommon->addMsg("Selected item has been deleted successfully.",1);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
}
$sql						 .= "SELECT * FROM pages WHERE 1 ";
if($search){
	$sql					.= " AND (page_title LIKE '%".$search."%' OR page_id LIKE '%".$search."%')";	
}
$sql						 .= " ORDER by page_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objPages->listQuery($paginationQuery);



?>
<div class="page-heading">
	<h3>Pages</h3>
	<ul class="breadcrumb">
		<li><a href="#">Pages</a></li>
		<li class="active"> Add Pages</li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            
            
            
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">Page List</header>
                    <div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-6 col-md-offset-6 ">
                                <form class="form-search pull-right" method="post" action="">
                                	<input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                                    <input type="hidden" name="page" value="add-pages" />
                               		 <button class="btn btn-primary search_submit" type="submit">Submit</button>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                        <form name="assign_form" id="assign_form" action="access/assign-widgets.php" method="post" onsubmit="return check_selection()">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">&nbsp;</th>
                                        <th width="5%">No</th>
                                        <th width="25%">Page</th>
										<th width="10%">ID</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($contentList)>0){
                                    $i=1;
									$count_no = 0;
                                    foreach($contentList as $list){$count_no++;?>
                                    <tr>
                                        <td><input name="list[]" id="list<?php echo $count_no; ?>" class="chkbx" value="<?php echo $objCommon->html2text($list['page_id']); ?>"type="checkbox" /></td>
                                      <td><?php echo $i; ?></td>
                                        <td><?php echo $objCommon->html2text($list['page_title']); ?></td>
										<td><?php echo $list['page_id']; ?></td>
                                        <td>
											<a href="index.php?page=add-pages&wId=<?php echo $list['page_id']; ?>" class="actionLink " title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;
											<a href="index.php?page=add-pages&dId=<?php echo $list['page_id']; ?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>&nbsp;
											
                                        </td>                	
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="4">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                            
                            
                            </form>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
                    </div>
                </section>

            </div>
            
            
            
        </div>
        
       