<?php 
@session_start();
include_once("../../class/widgets.php");
include_once("../../class/common_class.php");
$objWidgets		 =	new widgets();
$objCommon		 =	new common();
$objCommon->adminCheck();

$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['widget_title'])&&$_POST['widget_title']!=""){
	$_POST['widget_title']	=	$objCommon->esc($_POST['widget_title']);
	$_POST['widget_img']   =	$_FILES['widget_img']['name'];
	
	if($editId){
		
		$sql_data_array = array('widget_title' => $_POST['widget_title']);
						 
		$objWidgets->update($sql_data_array,"widget_id=".$editId);
		
		if($_FILES['widget_img']['name']!=""){	
			$ext	=	strtolower(pathinfo($_FILES['widget_img']['name'], PATHINFO_EXTENSION));
			$validImages	=	array("jpg","jpeg","gif","png");
			if(in_array($ext,$validImages)){
				//$name	=	$objCommon->getAlias($_POST['widget_img']).md5($widget_id);
				$name	= $_FILES['widget_img']['name'];
				if(file_exists("../widgets/".$name)){
					unlink("../widgets/".$name);
				}
				$extension = '.'.$ext;
				$name_real = explode($extension,$name);
				$name = $name_real[0];
				$_POST['widget_img']	=	$objCommon->addIMG($_FILES['widget_img'],"../../widgets/",$name,200,300,false);
			}
			$objWidgets->update($_POST,"widget_id=$editId");
		}
		
		$objCommon->addMsg("Widget updated successfully",1);
	}else{
		
		$objWidgets->insert($_POST);
		$widget_id = mysql_insert_id();
		
		if($_FILES['widget_img']['name']!=""){
				
			$ext	=	strtolower(pathinfo($_FILES['widget_img']['name'], PATHINFO_EXTENSION));
			$validImages	=	array("jpg","jpeg","gif","png");
			if(in_array($ext,$validImages)){
				//$name	=	$objCommon->getAlias($_POST['widget_img']).md5($widget_id);
				$name	= $_FILES['widget_img']['name'];
				if(file_exists("../widgets/".$name)){
					unlink("../widgets/".$name);
				}
				$extension = '.'.$ext;
				$name_real = explode($extension,$name);
				$name = $name_real[0];
				$_POST['widget_img']	=	$objCommon->addIMG($_FILES['widget_img'],"../../widgets/",$name,200,300,false);
				
			}
		}
		
		
	
		$objWidgets->update($_POST,"widget_id=$widget_id");
		$objCommon->addMsg("Widget added successfully",1);
	}
	header("location:../index.php?page=add-reason-widgets");
	exit();
}else{
	$objCommon->addMsg("Please enter widget name",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>