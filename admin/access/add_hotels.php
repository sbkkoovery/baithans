<?php 
@session_start();
include_once("../../class/hotels.php");
include_once("../../class/common_class.php");
$objHotels		 =	new hotels();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['h_name'])&&$_POST['h_name']!=""){
	$_POST['h_name']	=	$objCommon->esc($_POST['h_name']);
	$_POST['h_alias']   =	$objCommon->getAlias($_POST['h_name']);
	if($editId){
		$objHotels->update($_POST,"h_id=".$editId);
		$objCommon->addMsg("Hotels  updated successfully",1);
	}else{
		$objHotels->insert($_POST);
		$objCommon->addMsg("Hotels  added successfully",1);
	}
	header("location:../index.php?page=add-hotels");
	exit();
}else{
	$objCommon->addMsg("Please enter hotel name",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>