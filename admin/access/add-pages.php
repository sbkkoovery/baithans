<?php 
@session_start();
include_once("../../class/pages.php");
include_once("../../class/common_class.php");
$objPages		 =	new pages();
$objCommon		 =	new common();
$objCommon->adminCheck();

$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['page_title'])&&$_POST['page_title']!=""){
	$_POST['page_title']	=	$objCommon->esc($_POST['page_title']);
	
	if($editId){
		
		$sql_data_array = array('page_title' => $_POST['page_title'],'page_alias' => $_POST['page_alias'],'page_des' => $_POST['page_des']);
			
						 
		$objPages->update($sql_data_array,"page_id=".$editId);
		
		
		
		$objCommon->addMsg("Page updated successfully",1);
	}else{
		
		$objPages->insert($_POST);
		$page_id = mysql_insert_id();
		
		
	
		$objPages->update($_POST,"page_id=$page_id");
		$objCommon->addMsg("Page added successfully",1);
	}
	
	header("location:../index.php?page=add-pages");
	exit();
}else{
	$objCommon->addMsg("Please enter Page name",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>