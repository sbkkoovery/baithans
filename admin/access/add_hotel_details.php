<?php 
@session_start();
include_once("../../class/hotel_details.php");
include_once("../../class/hotel_sliders.php");
include_once("../../class/common_class.php");
$objHotelDetials   				 =	new hotel_details();
$objCommon		 		  	   =	new common();
$obj_hotel_sliders			   =	new hotel_sliders();
$objCommon->adminCheck();
$editId					 	  =	$objCommon->esc($_POST['h_id']);
$valid_formats 			   	   = 	array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
if(isset($editId) && $editId != ''){
	$getDetails			 	  =	$objHotelDetials->getRowSql("SELECT hd_id FROM hotel_details WHERE h_id=".$editId);
	$_POST['hd_place']	  	   =	$objCommon->esc($_POST['hd_place']);
	$_POST['hd_welcome_caption'] =	$objCommon->esc($_POST['hd_welcome_caption']);
	$_POST['hd_welcome_descr']   =	$objCommon->esc($_POST['hd_welcome_descr']);
	$_POST['hd_highlights']      =	$objCommon->esc($_POST['hd_highlights']);
	
	
	if($getDetails['hd_id']){
		$objHotelDetials->update($_POST,"hd_id=".$getDetails['hd_id']);
		$objCommon->addMsg("Hotel details updated successfully",1);
	}else{
		$_POST['hd_created']  	 =	date("Y-m-d H:i:s");
		$objHotelDetials->insert($_POST);
		$objCommon->addMsg("Hotel details added successfully",1);
	}
	
	if($_FILES['hd_logo']['name']){
		$pathLogo	 	 =	'../../uploads/hotels/logo/';
		$todayDate		=	date('Y-m-d');
		if(!file_exists($pathLogo.$todayDate)){
			mkdir($pathLogo.$todayDate,777);
		}
		$pathLogo		 =	$pathLogo.'/';
		$name 			 =	$_FILES['hd_logo']['name'];
		$size 			 =	$_FILES['hd_logo']['size'];
		if(strlen($name))
		{
			$ext		  =	strtolower(pathinfo($name, PATHINFO_EXTENSION));
		if(in_array($ext,$valid_formats))
		{
		if($size<(3072*10240))
		{
		$time = time();			
		echo $actual_image_name_ext_no	=	$editId;
		$actual_image_name 		   = 	$time.".".$ext;
		
		$tmpName = $_FILES['hd_logo']['tmp_name'];     
		if(move_uploaded_file($_FILES['hd_logo']['tmp_name'], $pathLogo.$actual_image_name)){
			$objHotelDetials->updateField(array("hd_logo"=>$actual_image_name),"h_id=".$editId);
		}
		}
		}
		}	
	}
	if($_FILES['hd_home']['name']){
		$pathHome	 	 =	'../../uploads/hotels/home_image/';
		$todayDate		=	date('Y-m-d');
		if(!file_exists($pathHome.$todayDate)){
			mkdir($pathHome.$todayDate);
		}
		$pathHome		 =	$pathHome.'/';
		$name 			 =	$_FILES['hd_home']['name'];
		$size 			 =	$_FILES['hd_home']['size'];
		if(strlen($name))
		{
			$ext		  =	strtolower(pathinfo($name, PATHINFO_EXTENSION));
		if(in_array($ext,$valid_formats))
		{
		if($size<(3072*10240))
		{	
		$time = time();		
		$actual_image_name_ext_no	=	$editId;
		$actual_image_name 		   = 	$time.".".$ext;
		
		$tmpName = $_FILES['hd_home']['tmp_name'];     
		if(move_uploaded_file($_FILES['hd_home']['tmp_name'], $pathHome.$actual_image_name)){
			$objHotelDetials->updateField(array("hd_home"=>$actual_image_name),"h_id=".$editId);
		}
		}
		}
		}	
	}
	if(count($_FILES['hd_bg']['name'])>0){
		foreach($_FILES['hd_bg']['tmp_name'] as $i => $tmp_name ){ 
			$pathSlider	 	 =	'../../uploads/hotels/slider/';
			$todayDate		  =	date('Y-m-d');
			/*if(!file_exists($pathSlider.$todayDate)){
				//echo $pathSlider.$todayDate;die;
				mkdir($pathSlider.$todayDate);
			}*/
			$pathSlider	   =	$pathSlider.'/';
			$name 			 =	$_FILES['hd_bg']['name'][$i];
			$size 			 =	$_FILES['hd_bg']['size'][$i];
			if(strlen($name))
			{
				$ext		  =	strtolower(pathinfo($name, PATHINFO_EXTENSION));
			if(in_array($ext,$valid_formats))
			{
			if($size<(3072*10240))
			{
			$time						=	time();				
			$actual_image_name 		   = 	$time."_".$i."_".".".$ext;
			
			$tmpName = $_FILES['hd_bg']['tmp_name'][$i];     
			if(move_uploaded_file($_FILES['hd_bg']['tmp_name'][$i], $pathSlider.$actual_image_name)){
				$_POST['hs_img']		 =	$actual_image_name;
				$_POST['h_id']		   =	$editId;
				$obj_hotel_sliders->insert($_POST);
			}
			}
			}
			}
		}
	}

	header("location:".$_SERVER['HTTP_REFERER']);
	exit();
}else{
	$objCommon->addMsg("Please enter hotel name",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>