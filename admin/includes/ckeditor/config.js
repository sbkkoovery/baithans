﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	config.enterMode = CKEDITOR.ENTER_BR;
config.shiftEnterMode = CKEDITOR.ENTER_BR;
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
	 config.filebrowserBrowseUrl ='includes/ckeditor/filemanager/browser/default/browser.html?Connector=http://localhost/baithans/admin/includes/ckeditor/filemanager/connectors/php/connector.php',
     config.filebrowserImageBrowseUrl = 'includes/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=http://localhost/baithans/admin/includes/ckeditor/filemanager/connectors/php/connector.php',
     config.filebrowserFlashBrowseUrl ='includes/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=http://localhost/baithans/admin/includes/ckeditor/filemanager/connectors/php/connector.php',
	 config.filebrowserUploadUrl  ='http://localhost/baithans/admin/includes/ckeditor/filemanager/connectors/php/upload.php?Type=File',
	 config.filebrowserImageUploadUrl = 'http://localhost/baithans/admin/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Image',
	 config.filebrowserFlashUploadUrl = 'http://localhost/baithans/admin/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
};
