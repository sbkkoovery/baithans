<?php 
@session_start();
ob_start();
include_once("../class/common_class.php");
include_once("../class/pagination-class.php");
$objCommon	=	new common();
if(!$objCommon->adminLogin()){
	header("location:login.php");
	exit();
}
$page	=	"dashboard.php";
if(isset($_GET['page'])&&$_GET['page']!=""){
	$page	=	$_GET['page'];
}
$pageName	=	"pages/".$page.".php";
if(!file_exists($pageName)){
	$pageName	=	"pages/dashboard.php";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link rel="shortcut icon" href="#" type="image/png">
  <title>Admin Baithans</title>

  <!--icheck-->
  <link href="js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
  <link href="js/iCheck/skins/square/square.css" rel="stylesheet">
  <link href="js/iCheck/skins/square/red.css" rel="stylesheet">
  <link href="js/iCheck/skins/square/blue.css" rel="stylesheet">
  <script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="ckfinder/ckfinder.js"></script>

  <!--dashboard calendar-->
  <link href="css/clndr.css" rel="stylesheet">


  <!--common-->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">




  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">
<section>
    <!-- left side start-->
    <div class="left-side sticky-left-side">

        <!--logo and iconic logo start-->
        <div class="logo">
            <a href="index.html"><img src="images/logo.png" alt="" width="110"></a>
        </div>

        <div class="logo-icon text-center">
            <a href="index.html"><img src="images/logo_icon.png" alt=""></a>
        </div>
        <!--logo and iconic logo end-->

        <div class="left-side-inner">

            <!-- visible to small devices only -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media logged-user">
                    <img alt="" src="images/photos/user-avatar.jpg" class="media-object">
                    <div class="media-body">
                        <h4><a href="#">Baithans</a></h4>
                        <span>"Hello There..."</span>
                    </div>
                </div>
                <h5 class="left-nav-title">Account Information</h5>
                <ul class="nav nav-pills nav-stacked custom-nav">
                  <li><a href="#"><i class="fa fa-user"></i> <span>Profile</span></a></li>
                  <li><a href="#"><i class="fa fa-cog"></i> <span>Settings</span></a></li>
                  <li><a href="#"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
                </ul>
            </div>

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li class="active"><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

                <li><a href=""><i class="fa fa-home"></i> <span>Visit Site</span></a></li>
                <li class="menu-list <?php echo ($page=='add-hotels' ||$page=='add-rooms' || $page=='add-hotel-details')?'nav-active':'';?>"><a href=""><i class="fa fa-file-text"></i> <span>Hotels</span></a>
                    <ul class="sub-menu-list">
                        <li <?php echo ($page=='add-hotels' ||$page=='add-rooms' || $page=='add-hotel-details')?'class="active"':'';?>><a href="index.php?page=add-hotels"> Add Hotels</a></li>
                    </ul>
                </li>
                
                <li class="menu-list"><a href=""><i class="fa fa-users"></i> <span>Users</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="general.html"> All Users</a></li>
                        <li><a href="general.html"> Unverified Users</a></li>
                        <li><a href="buttons.html"> User Categories</a></li>
                        <li><a href="tabs-accordions.html"> Add Category</a></li>
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-picture-o"></i> <span>Media Library</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="grids.html"> Images</a></li>
                        <li><a href="gallery.html"> Videos</a></li>
                        <li><a href="mail_compose.html"> Abused Medeas</a></li>
                    </ul>
                </li>
                
                <li class="menu-list <?php echo ($page=='reason-widgets')?'nav-active':'';?>"><a href=""><i class="fa fa-file-text"></i> <span>Reason Widgets</span></a>
                    <ul class="sub-menu-list">
                        <li <?php echo ($page=='add-reason-widgets')?'class="active"':'';?>><a href="index.php?page=add-reason-widgets"> Add Widgets</a></li>
                        <li <?php echo ($page=='assign-reason-widgets')?'class="active"':'';?>><a href="index.php?page=assign-reason-widgets"> Assign Widgets</a></li>
                    </ul>
                </li>
                
                <li class="menu-list <?php echo ($page=='reason-widgets')?'nav-active':'';?>"><a href=""><i class="fa fa-file-text"></i> <span>Pages</span></a>
                    <ul class="sub-menu-list">
                        <li <?php echo ($page=='add-pages')?'class="active"':'';?>><a href="index.php?page=add-pages"> Add Pages</a></li>
                        
                        <li <?php echo ($page=='view-pages')?'class="active"':'';?>><a href="index.php?page=view-pages"> View Pages</a></li>
                    </ul>
                </li>
                
            </ul>
            <!--sidebar nav end-->

        </div>
    </div>
    <!-- left side end-->
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <!--toggle button start-->
            <a class="toggle-btn"><i class="fa fa-bars"></i></a>
            <!--toggle button end-->
            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu"> 
                    <li class="paddRight10">
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="images/photos/user-avatar.jpg" alt="" />
                            Baithans
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                            <li><a href="#"><i class="fa fa-user"></i>  Profile</a></li>
                            <li><a href="#"><i class="fa fa-cog"></i>  Settings</a></li>
                            <li><a href="index.php?page=log-out"><i class="fa fa-sign-out"></i> Log Out</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->



        <!--body wrapper start-->
        <div class="wrapper">
        <?php include_once($pageName); ?>
        </div>
        <!--body wrapper end-->

        <!--footer section start-->
        <footer class="sticky-footer">
            <?php echo date("Y");?> &copy; Baithans.com
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>

<!--easy pie chart-->
<script src="js/easypiechart/jquery.easypiechart.js"></script>
<script src="js/easypiechart/easypiechart-init.js"></script>

<!--Sparkline Chart-->
<script src="js/sparkline/jquery.sparkline.js"></script>
<script src="js/sparkline/sparkline-init.js"></script>

<!--icheck -->
<script src="js/iCheck/jquery.icheck.js"></script>
<script src="js/icheck-init.js"></script>

<!-- jQuery Flot Chart-->
<script src="js/flot-chart/jquery.flot.js"></script>
<script src="js/flot-chart/jquery.flot.tooltip.js"></script>
<script src="js/flot-chart/jquery.flot.resize.js"></script>
<script src="js/flot-chart/jquery.flot.pie.resize.js"></script>
<script src="js/flot-chart/jquery.flot.selection.js"></script>
<script src="js/flot-chart/jquery.flot.stack.js"></script>
<script src="js/flot-chart/jquery.flot.time.js"></script>
<script src="js/main-chart.js"></script>

<!--common scripts for all pages-->
<script src="js/scripts.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
</body>
</html>
