<?php 
	include_once('includes/header.php');
?>
<link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'>
<script src="js/hotel.js" type="text/javascript"></script>
<link href="css/hotel.css" rel="stylesheet" type="text/css" />
<style>
	body{
		overflow:hidden;
	}
</style>
<div class="main-container">
    <div class="slider-right">
        <div class="slider" id="slide">
            <ul class="slideme">
                <li><img src="images/bg2.jpg" /></li>
                <li><img src="images/bg3.jpg" /></li>
                <li><img src="images/bg7.jpg" /></li>
            </ul>
        </div>
    </div>
    <div class="ovelay-container"></div>
    	<div class="lognd">
        	<div class="logo-hotels">
            	<img class="center-block" src="images/logo-center1.png" />
            </div>
            <div class="welcome text-center">
            	<h2><span>Welcome To</span><br/>Dana Hotel Sharjah</h2>
                <p class="sub-tag">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            </div>
        </div>
        <ul class="nav-bottm" id="nav-bottm">
        	<li onclick="loademe('ajax/facility.php');">Facilities</li>
            <li onclick="loademe('ajax/booking.php');">Hospitality</li>
            <li onclick="loademe('ajax/gallery.php');">Gallery</li>
            <li id="no-nav" onclick="loademe('ajax/reservation.php');">Reservation</li>
            <li onclick="loademe('ajax/contact.php');">Contact Us</li>
        </ul>
        <div class="about-us" id="about-us">
        	<div class="close-bounce">
            	<img src="images/close.png" width="22" />
            </div>
            <div class="load-contents" id="load-contents">
            	<div class="sk-spinner sk-spinner-wave inside" id="inside">
                  <div class="sk-rect1"></div>
                  <div class="sk-rect2"></div>
                  <div class="sk-rect3"></div>
                  <div class="sk-rect4"></div>
                  <div class="sk-rect5"></div>
                </div>
                <div class="load_here" id="load_here">
                </div>
            </div>
        </div>
</div>
    <script type="text/javascript">
		$(document).ready(function(){
			$('#slide').slideme({
				autoslide : true,
				loop : true,
				interval : 2000,
				speed : 1000,
				arrows: false,
				autoslideHoverStop : false,
				css3 : true,
				transition : 'fade',
				touch : true
				});
			})
	</script>
<?php 
	include_once('includes/footer.php');
?>