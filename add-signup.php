<?php 
@session_start();
include_once("class/common_class.php");
include_once("class/user.php");
require_once("PHPMailer/class.phpmailer.php");

$objCommon	  =	new common();
$objuser		=	new user();
$mail = new PHPMailer();
$mail->CharSet = "UTF-8";
$hash = md5(rand(0,1000));

if (isset($_POST['name']) && isset($_POST['password']) && isset($_POST['email'])){
	$pwd = $objCommon->esc($_POST['password']);
	$_POST['name']	=	$objCommon->esc($_POST['name']);
	$_POST['email']	=	$objCommon->esc($_POST['email']);
	$_POST['password']	=	md5($objCommon->esc($_POST['password']));
	$_POST['phone']	=	$objCommon->esc($_POST['phone']);
	$_POST['location']	=	$objCommon->esc($_POST['location']);
	$_POST['hash']	=	$hash;
	$_POST['active']	=	$objCommon->esc($_POST['active']);
	//print_r($_POST); die;
	$objuser->insert($_POST);
	$objCommon->addMsg("Your account has been made, <br /> please verify it by clicking the activation link that has been send to your email",1);
	
	//For Mail
   // $mail->isSMTP();                                      // Set mailer to use SMTP
   // $mail->Host = 'mail.designdays.ae';  // Specify main and backup SMTP servers
   // $mail->SMTPAuth = true;                               // Enable SMTP authentication
   // $mail->Username = 'rajeshp@designdays.ae';                 // SMTP username
   // $mail->Password = 'Rajesh@123';                           // SMTP password
   // $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
   // $mail->Port = 587;                                    // TCP port to connect to

    $mail->setFrom('info@designdays.ae', 'Info');
    $mail->addAddress($_POST['email'], $_POST['name']);     // Add a recipient
                   
    $mail->addReplyTo('info@designdays.ae', 'Information');
    //$mail->addCC('rajeshp@designdays.ae');
    $mail->addBCC('rajeshp@designdays.ae');

    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = 'Account Activation Mail';
	$message = '
 
Thanks for signing up!
Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.<br/><br/>
 
------------------------<br/>
Username: '.$_POST['email'].'<br/>
Password: '.$pwd.'<br/>
------------------------<br/><br/>
 
Please click this link to activate your account:<br/>
http://designdays.ae/baithans/live/verify.php?email='.$_POST['email'].'&hash='.$hash.''; 
    $mail->Body    = $message;
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
   	if(!$mail->send()) {
        echo 'Mail could not be sent.';
        echo 'Mail Error: ' . $mail->ErrorInfo;
    } else {
       // echo 'Mail has been sent';
	   header("location:index.php");
	 }
	//end of mail 
	    header("location:index.php");
		exit();
}else{
	$objCommon->addMsg("Please fill fields..,",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>