<?php 
@session_start();
include_once("class/common_class.php");
include_once("class/user.php");
require_once("PHPMailer/class.phpmailer.php");
include_once("class/bookings.php");
include_once('class/hotels.php');
include_once('class/bookdetails.php');
include_once("class/hotel_rooms.php");

if(isset($_SESSION[user][userId])){
$uname = $_SESSION[user][userName];
$uid = $_SESSION[user][userId]; 
$email = $_SESSION[user][email];
}

$objCommon	  =	new common();
$objuser		=	new user();
$objBookings    =    new bookings();
$objHotels	  =	new hotels();
$objBDetails    =    new bookdetails();
$objHotelrooms  =	new hotel_rooms();

$mail = new PHPMailer();
$mail->CharSet = "UTF-8";

if (isset($_GET['date1']) && isset($_GET['date2']) && isset($_GET['hid']) && isset($_GET['roomid']) && isset($_GET['bid'])){
	$_POST['room_id']	 =	 $objCommon->esc($_GET['roomid']);
	$_POST['h_id']	    =	 $objCommon->esc($_GET['hid']);
	$_POST['user_id']	 =	 $uid;
	$_POST['checkin']	 =	 $objCommon->esc($_GET['date1']);
	$_POST['checkout']	=	 $objCommon->esc($_GET['date2']);
	$bookingId	        =	 $objCommon->esc($_GET['bid']);
	$dtime1 = date("m/d/Y",$_GET['date1']);
	$dtime2 = date("m/d/Y",$_GET['date2']);  
	$checkindate  = date("M jS, Y", strtotime($dtime1));
	$checkoutdate = date("M jS, Y", strtotime($dtime2));
	
	$hid	    =	 $objCommon->esc($_GET['hid']);
	$getHotelDetails	   =	$objHotels->listQuery("SELECT hotel.*
												       FROM hotels AS hotel
													   WHERE hotel.h_id=".$hid);
	if(count($getHotelDetails)>0){
		foreach($getHotelDetails as $hotel){ 
		   $hname = $objCommon->html2text($hotel['h_name']);
		}}
	$getRooms	          =	$objHotelrooms->listQuery("SELECT r.*
												       FROM hotel_rooms AS r
													   WHERE r.room_id=".$objCommon->esc($_GET['roomid']));
	if(count($getRooms)>0){
		foreach($getRooms as $hotelrooms){ 
		   $roomtype = $objCommon->html2text($hotelrooms['room_type']);
		}}
	
	
	//print_r($_POST); die;
	$objBookings ->insert($_POST); 
	$last_id = mysql_insert_id(); 
	$objBDetails->updateField(array("b_id"=>$last_id),"bookdetails_id=".$bookingId);
	$objCommon->addMsg("Your booking has been made, <br /> please check your mail for details",1);
	
	$mail->setFrom('info@designdays.ae', 'Booking Info');
    $mail->addAddress($email, $uname);     // Add a recipient
    $mail->addReplyTo('info@designdays.ae', 'Information');
    //$mail->addCC('rajeshp@designdays.ae');
    $mail->addBCC('rajeshp@designdays.ae');

    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Booking Info Mail';
	$message = '
	 
Dear '.$uname.',<br/><br/>
Thank you for choosing '.$hname.' as your home away from home. It is our pleasure to confirm your reservation. Please dont hesitate to contact us with any changes.<br/><br/>

    
------------------------Reservation Details---------------------<br/>
Confirmation Number:'.$bookingId.' <br/>
Guest Name:'.$uname.' <br/>
Arrival Date:'.$checkindate.' <br/>
Departure Date:'.$checkoutdate.' <br/>
Room Type:'.$roomtype.' <br/>
----------------------------------------------------------------<br/><br/>
 
Best Regards,<br/>
The Manager,<br/>
'.$hname.''; 
    $mail->Body    = $message;
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
   	if(!$mail->send()) {
        echo 'Mail could not be sent.';
        echo 'Mail Error: ' . $mail->ErrorInfo;
    } else {
       // echo 'Mail has been sent';
		header("location:ajax/bookconform.php");
    }
	//end of mail 	
	
	
		header("location:ajax/bookconform.php");
		exit();
}else{
	$objCommon->addMsg("Please fill fields..,",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>
	
	