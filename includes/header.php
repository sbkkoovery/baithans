<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Baithans | Welcome</title>
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="css/perfect-scrollbar.min.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Roboto:500,900,100,300,700,400' rel='stylesheet' type='text/css'>
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/supersized.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/supersized.shutter.css" type="text/css" media="screen" />
<link href="css/slideme.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script type="text/javascript">
	jQuery(window).load(function() {
	 jQuery("#status").fadeOut();
	 jQuery("#preloader").delay(1000).fadeOut("slow");
});

</script>
</head>
<body>
<div id="preloader">
     <div class="sk-spinner sk-spinner-wave">
      <div class="sk-rect1"></div>
      <div class="sk-rect2"></div>
      <div class="sk-rect3"></div>
      <div class="sk-rect4"></div>
      <div class="sk-rect5"></div>
    </div>
</div>
<div class="pop-bg-book" id="pop-bg-book">
        	<div class="container-popup">
            	<div class="container-inside-popup text-center">
                	<div class="head_popup">
                        	<p>Find The Best Rate</p>
                        </div>
                        	<form>
                            <div class="calender-sec">
                            	<div class="checkin">
                                        	<p>Check in</p>
                                        </div>
                                        <div class="date-piker">
                                   <input type="text" class="datepicker" />
                                   </div>
                                   <div class="checkin">
                                        	<p>Check out</p>
                                        </div>
                                        <div class="date-piker">
                                   <input type="text" class="datepicker" />
                                   </div>
                                   <div class="selct-option">
                                   <p>Rooms</p>
                                   		<select>
                                        	<option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                   </div>
                                   <div class="selct-option">
                                   <p>Adults</p>
                                   		<select>
                                        	<option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                   </div>
                                   <div class="selct-option">
                                   <p>Children</p>
                                   		<select>
                                        	<option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                   </div>
                                   <div class="help">
                                   		<a href="#"></a>
                                   	</div>
                                     <div class="clearfix"></div>
                                    <p class="per-room">Per room</p>
                                   <div class="best-rate">
                                   	<a href="#">Find The Best Rate <i class="fa fa-angle-right"></i></a>
                                   </div>
                                   <div class="imgtick">
                                   		<span class="img-learn"><img src="images/tick.png" /></span><a href="#">Learn about our best rate guarantee</a>
                                   </div>
                                   <div class="clearfix"></div>
                               </div>
                            </form>
                   </div>
            </div>
        </div>