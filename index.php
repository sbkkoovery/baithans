<?php 
@session_start();
    include_once('class/common_class.php');
	include_once('includes/header.php');	
	include_once('class/hotels.php');
	$objHotels			  	   =	new hotels();
	$objCommon		 		   =	new common();
	if(isset($_SESSION['user'])){
	$username	=	$_SESSION['user']["userName"]; 
	}
	$getHotelDetails	   =	$objHotels->listQuery("SELECT hotel.*,det.*
												       FROM hotels AS hotel
													   LEFT JOIN hotel_details AS det ON hotel.h_id = det.h_id");
?>
<script type="text/javascript" src="js/jquery.easing.min.js"></script>
<script type="text/javascript" src="js/supersized.3.2.7.min.js"></script>
<script type="text/javascript" src="js/supersized.shutter.min.js"></script>
<script type="text/javascript">
			jQuery(function($){
				$.supersized({
				// Functionality
					slide_interval          :   3000,		// Length between transitions
					transition              :   1, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
					transition_speed		:	700,		// Speed of transition
					// Components							
					slide_links				:	'blank',	// Individual links for each slide (Options: false, 'num', 'name', 'blank')
					slides 					:  	[			// Slideshow Images
														{image : 'images/bg2.jpg', title : 'Hala In', thumb : 'images/bg2.jpg', url : 'images/bg2.jpg'},
														{image : 'images/bg3.jpg', title : 'Baithans', thumb : 'images/bg2.jpg', url : 'images/bg3.jpg'},  
														{image : 'images/bg4.jpg', title : 'Baithans Realestate', thumb : 'images/bg2.jpg', url : 'images/bg4.jpg'},
												]
					
				});
		    });
		    
		</script>
<style>
body{
	overflow:hidden;
	outline:none;
	-webkit-cursor:grab;
	-moz-cursor:grab;
	-os-cursor:grab;
	cursor:grab;
}
</style>
<div id="thumb-tray" class="load-item">
		<div id="thumb-back"></div>
		<div id="thumb-forward"></div>
	</div>
	<!--Time Bar-->
	<div id="progress-back" class="load-item">
		<div id="progress-bar"></div>
	</div>
	<!--Control Bar-->
	<div id="controls-wrapper" class="load-item">
		<div id="controls">
		<a id="play-button"><img id="pauseplay" src="img/pause.png"/></a>
		<!--Slide counter-->
			<div id="slidecounter">
				<span class="slidenumber"></span> / <span class="totalslides"></span>
			</div>
				<!--Slide captions displayed here-->
			<div id="slidecaption"></div>
			</div>
	</div>

    <div class="bg-cover"></div>
    	<div class="main-container-over">
        	<div class="video-pic" id="video-pic">
        	<div class="img-sec">
            	<img class="center-block" src="images/pic.png" />
            </div>
            <div class="desc text-center">
            	<p class="main-desc"></p>
                <p class="second-desc"></p>
            </div>
        </div>
        	<div class="hotel-logo hidden-xs" id="hotel-logo">
            	<img class="img-responsive center-block" src="images/logo-center.png" />
            </div>
            <div class="wecome-msg-d text-center hidden-xs" id="wecome-msg-d">
        	<h1>WELCOME TO<br/><span class="bolt-it-m">BAITHANS GROUP</span></h1>
            <p class="tagline">We Provide Most Luxury Vacations</p>
        </div>
        <div class="wecome-msg text-center hidden-xs " id="wecome-msg" style="display:none;">
        	<h1>WELCOME TO<br/><span class="bolt-it">BAITHANS GROUP</span></h1>
            <p class="tagline">We Provide Most Luxury Vacations</p>
        </div>
       <div class="sidebar" id="togglesidebar">
        	<div class="sidebar-outside" id="slideclick">
            	<div id="menu-toggle">
                </div>
                <div class="down_options">
                	<!--<div class="lang" id="lang">
                    	<p>ENG</p>
                    </div>
                    <div class="sound">
                    	<i class="fa fa-volume-up"></i>
                    </div>-->
                    <div class="social-links">
                    	<img src="images/arrow.png" width="22" />
                    </div>
                </div>
            </div>
            <div class="sidebar-inside" id="slideonclick">
            <div id="closebtn">
            	<img class="img-responsive" src="images/close.png" />
            </div>
            <div id="navigationbar" class="navigationbar text-center">
                	<ul>
                    	<li><a href="index.php">Home</a></li>
                        <li onClick="loadcontent('ajax/aboutus.php');"><a href="Javascript:void(0);">About US</a></li>
                        <li onClick="loadcontent('ajax/history.php');"><a href="Javascript:void(0);">Our History</a></li>
                        <li onClick="loadcontent('ajax/contactus.php');"><a href="Javascript:void(0);">Contact US</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-overlay" id="bg-overlay"></div>
        <div class="load-page" id="load-page">
        	<div id="load-pages">
            </div>
        	<div id="loading-image"><img src="images/pre_loader.gif" width="32" /></div>
        </div>
        
        <div class="log-left">
        	<a href="index.php"><img src="images/logo.png" /></a>
        </div>
         <div id="mobile-menu" class="mobile-menu hidden-sm hidden-md hidden-lg"></div>
        
        <div id="menu-right" class="menu-right pull-right "> 
        <div id="close-mob" class="close-mob hidden-sm- hidden-md hidden-lg"></div>
         <a href="#" class="bookmark">Book Now</a>
             <?php
			if(isset($username)){?>
				<a href="logout.php" class="logout">Log Out <?php echo $username;?> </a>
			<?php }else{?>
            
            <a href="#" class="login">Log In</a>
             <?php }?>
           <!-- <a href="#" class="bookmark">Book Now</a>
            <a href="#" class="login">Log In</a>-->
        </div>
        <div id="holding-container" class="holding-container">
       
        <!--<div id="mid-items">
        <div class="item-container">
      		<div class="box-single">
                 <input type="hidden"  value=" HALA IN" id="takevalue"/>
                  <input type="hidden"  value=" Spend luxury time with Hala In" id="tagline"/>
                  <img id="logoval" src="images/logo-center.png" style="display:none;" />
                <div class="box-items"  style="background-image:url('images/bg2.jpg');"><a href="hotels.php"></a><p class="hotel-name">Hala In</p>
                	<div class="rectangle-block">
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                <div class="video-pic-1" style="display:none;">
                    <div class="img-sec-change">
                        <img class="center-block" src="images/pic.png" />
                    </div>
                     <p class="changed-head">Photos</p>
                        <p class="changed-desc">When something happends</p>
                 </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/video.png" />
                        </div>
                        <p class="changed-head">Videos</p>
                        <p class="changed-desc">Its working</p>
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/book.png" />
                        </div>
                        <p class="changed-head">Booking</p>
                        <p class="changed-desc">Agin its booking</p>
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/chat.png" />
                        </div>
                        <p class="changed-head">Chats</p>
                        <p class="changed-desc">Oh! its chats..!</p>
                    </div>
                </div>
            </div>
            <div class="box-single">
                  <input type="hidden"  value=" DANA HOTEL SHARJAH" id="takevalue"/>
                  <input type="hidden"  value="Spend luxury time with Dana Hotel" id="tagline"/>
                  <img id="logoval" src="images/logo-center1.png" style="display:none;" />
                <div class="box-items"  style="background-image:url('images/bg3.jpg');"><a href="hotels.php"></a><p class="hotel-name">Dana Hotel</p>
                	<div class="rectangle-block">
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                <div class="video-pic-1" style="display:none;">
                    <div class="img-sec-change">
                        <img class="center-block" src="images/pic.png" />
                    </div>
                     <p class="changed-head">Photos</p>
                        <p class="changed-desc">When something happends</p>
                 </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/video.png" />
                        </div>
                        <p class="changed-head">Videos</p>
                        <p class="changed-desc">Its working</p>
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/book.png" />
                        </div>
                        <p class="changed-head">Booking</p>
                        <p class="changed-desc">Agin its booking</p>
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/chat.png" />
                        </div>
                        <p class="changed-head">Chats</p>
                        <p class="changed-desc">Oh! its chats..!</p>
                    </div>
                </div>
                </div>
                <div class="box-single">
                  <input type="hidden"  value=" SAFARI HOTEL APPARTMENTS" id="takevalue"/>
                  <input type="hidden"  value="Spend luxury time with Safari Hotels" id="tagline"/>
                  <img id="logoval" src="images/logo-center2.png" style="display:none;" />
                <div class="box-items"  style="background-image:url('images/bg4.jpg');"><a href="hotels.php"></a><p class="hotel-name">Safari Hotels</p>
                	<div class="rectangle-block">
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                <div class="video-pic-1" style="display:none;">
                    <div class="img-sec-change">
                        <img class="center-block" src="images/pic.png" />
                    </div>
                     <p class="changed-head">Photos</p>
                        <p class="changed-desc">When something happends</p>
                 </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/video.png" />
                        </div>
                        <p class="changed-head">Videos</p>
                        <p class="changed-desc">Its working</p>
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/book.png" />
                        </div>
                        <p class="changed-head">Booking</p>
                        <p class="changed-desc">Agin its booking</p>
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/chat.png" />
                        </div>
                        <p class="changed-head">Chats</p>
                        <p class="changed-desc">Oh! its chats..!</p>
                    </div>
                </div>
                </div>
                <div class="box-single">
                  <input type="hidden"  value=" SARA HOTEL APPARTMENTS" id="takevalue"/>
                  <input type="hidden"  value="Spend luxury time with Sara Hotels" id="tagline"/>
                  <img id="logoval" src="images/logo-center.png" style="display:none;" />
                <div class="box-items"  style="background-image:url('images/bg9.jpg');"><a href="hotels.php"></a><p class="hotel-name">Sara Hotels</p>
                	<div class="rectangle-block">
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                <div class="video-pic-1" style="display:none;">
                    <div class="img-sec-change">
                        <img class="center-block" src="images/pic.png" />
                    </div>
                     <p class="changed-head">Photos</p>
                        <p class="changed-desc">When something happends</p>
                 </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/video.png" />
                        </div>
                        <p class="changed-head">Videos</p>
                        <p class="changed-desc">Its working</p>
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/book.png" />
                        </div>
                        <p class="changed-head">Booking</p>
                        <p class="changed-desc">Agin its booking</p>
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/chat.png" />
                        </div>
                        <p class="changed-head">Chats</p>
                        <p class="changed-desc">Oh! its chats..!</p>
                    </div>
                </div>
                </div>
                <div class="box-single">
                  <input type="hidden"  value=" PAN EMIRITES HOTEL SHARJAH" id="takevalue"/>
                  <input type="hidden"  value="Spend luxury time with Pan Emirites" id="tagline"/>
                  <img id="logoval" src="images/logo-center4.png" style="display:none;" />
                <div class="box-items"  style="background-image:url('images/bg1.jpg');"><a href="hotels.php"></a><p class="hotel-name"> Pan Emirites</p>
                	<div class="rectangle-block">
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                <div class="video-pic-1" style="display:none;">
                    <div class="img-sec-change">
                        <img class="center-block" src="images/pic.png" />
                    </div>
                     <p class="changed-head">Photos</p>
                        <p class="changed-desc">When something happends</p>
                 </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/video.png" />
                        </div>
                        <p class="changed-head">Videos</p>
                        <p class="changed-desc">Its working</p>
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/book.png" />
                        </div>
                        <p class="changed-head">Booking</p>
                        <p class="changed-desc">Agin its booking</p>
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/chat.png" />
                        </div>
                        <p class="changed-head">Chats</p>
                        <p class="changed-desc">Oh! its chats..!</p>
                    </div>
                </div>
                </div>
                <div class="box-single">
                  <input type="hidden"  value=" BAITHANS REAL ESTATE" id="takevalue"/>
                  <input type="hidden"  value="Be a part of Baithans Real Estate" id="tagline"/>
                  <img id="logoval" src="images/logo-center5.png" style="display:none;" />
                <div class="box-items"  style="background-image:url('images/bg6.jpg');"><a href="hotels.php"></a><p class="hotel-name"> Real Estate</p>
                	<div class="rectangle-block">
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                <div class="video-pic-1" style="display:none;">
                    <div class="img-sec-change">
                        <img class="center-block" src="images/pic.png" />
                    </div>
                     <p class="changed-head">Photos</p>
                        <p class="changed-desc">When something happends</p>
                 </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/video.png" />
                        </div>
                        <p class="changed-head">Videos</p>
                        <p class="changed-desc">Its working</p>
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/book.png" />
                        </div>
                        <p class="changed-head">Booking</p>
                        <p class="changed-desc">Agin its booking</p>
                    </div>
                </div>
                <div class="rectangles">
                <div class="hove-me"></div>
                    <div class="video-pic-1" style="display:none;">
                        <div class="img-sec-change">
                            <img class="center-block" src="images/chat.png" />
                        </div>
                        <p class="changed-head">Chats</p>
                        <p class="changed-desc">Oh! its chats..!</p>
                    </div>
                </div>-->
                </div>
           
    
    
    <!-----Changes Starts.....Carsouel Slider for sliding hotel list ---->  
    
     <!---
    
    	slider init javasctipt is included Bottom,
        Line Number : 24 ~ 41
    -->     
           <div class="hotels_baithans">
           		<div class="HotelListSlider">
                  <?php if(count($getHotelDetails)>0){
			//print_r($getHotelDetails); die;
           foreach($getHotelDetails as $hotel){ 
		   $place = $objCommon->html2text($hotel['hd_place']);
		   $im = $objCommon->html2text($hotel['hd_home']);
		   if(($im)||($im!='NULL')){
		   $dt = $objCommon->html2text($hotel['hd_created']);
		   $imgs = 'uploads/hotels/home_image/'.$im; }
		   $lgo = $objCommon->html2text($hotel['hd_logo']);
		   if($lgo){
		   		   $logo = 'uploads/hotels/logo/'.$lgo;
				   }	
		   $hdd = $objCommon->html2text($hotel['h_id']);
		   ?>
					
                        <div class="hotels_listedBelow">
                        	<div class="img_hotel_section">
                            	<a href="#"><img src="<?=$imgs?>" /></a>
                                <a href="#"><span class="overlay_bg">
                                	<span class="helper"><img class="center-block" src="<?=$logo?>" /></span></a>
                            </div>
                            <div class="desc_hotel_baithans text-center">
                            	<a class="top_anchor" href="hotels.php?fid=<?php echo $hdd; ?>"><img src="images/anchor_red.jpg" /></a>
                            	<p><?=$place?></p>
                                <h4><?php echo $objCommon->html2text($hotel['h_name']); ?></h4>
                                <a class="vist_hotel_bathians" href="hotels.php?fid=<?php echo $hdd; ?>">Visit Hotel</a>
                            </div>
                        </div>
                    <?php }}?>
                </div>
           </div>
    <!-------End changes.....  Carsouel Slider for sliding hotel list --->     
           
           </div>
       </div>
       </div>
    </div>
    
    <!---
    
    	popup init javasctipt is included in ../js/main.js,
        Line Number : 599 ~ 624
    -->
    
    <!-----Booking Form popUp starts------>
    <div class="pop-bg-book" id="pop-bg-book">
        	<div class="container-popup">
            	<div class="container-inside-popup text-center">
                	<div class="head_popup">
                        	<p>Find Rooms</p>
                        </div>
                        	<form>
                            <div class="hotel_search text-left">
                                    <p>Hotel</p>
                                    <select>
                                    <option value="">Select Hotel</option>
                                    <?php if(count($getHotelDetails)>0){
			//print_r($getHotelDetails); die;
           foreach($getHotelDetails as $dhotel){ ?>
                                    		<option value="<?php echo $objCommon->html2text($dhotel['h_name']); ?>"><?php echo $objCommon->html2text($dhotel['h_name']); ?></option>
                                        	
                               <?php }}?>         
                                        </select>
                                </div>
                            <div class="calender-sec">
                            	<div class="checkin">
                                        	<p>Check in</p>
                                        </div>
                                        <div class="date-piker">
                                   <input type="text" class="datepicker" />
                                   </div>
                                   <div class="checkin">
                                        	<p>Check out</p>
                                        </div>
                                        <div class="date-piker">
                                   <input type="text" class="datepicker" />
                                   </div>
                                   <div class="selct-option">
                                   <p>Rooms</p>
                                   		<select>
                                        	<option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                   </div>
                                   <div class="selct-option">
                                   <p>Adults</p>
                                   		<select>
                                        	<option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                   </div>
                                   <div class="selct-option">
                                   <p>Children</p>
                                   		<select>
                                        	<option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                   </div>
                                   <?php /*?><div class="help">
                                   		<a href="#"></a>
                                   	</div><?php */?>
                                     <div class="clearfix"></div>
                                    <p class="per-room">Per room</p>
                                   <div class="best-rate">
                                   	<span onclick="loademe('ajax/reservation.php?hid=6');">Find Rooms <i class="fa fa-angle-right"></i></span>
                                   </div>
                                   <div class="clearfix"></div>
                               </div>
                            </form>
                   </div>
            </div>
        </div>
     <!-----End Booking form popUp starts------>
     
    <!-----Login Section popUp starts------>
    <div class="pop-bg-book" id="login">
        	<div class="container-popup">
            	<div class="container-inside-popup text-center">
                        <div class="login_session">
                           <span id="user-notexist-status" ></span>
                            <div class="head_popup">
                                <p>Login</p>
                            </div>
                        	<form class="form-signin" method="post" action="user_login.php" onsubmit="return(usercheck());">
                                   	<div class="form-group">
                                	<input type="text" name="usernames" id="usernames" class="form-control" placeholder="Username or Email Address" />
                                </div>
                            	<div class="form-group">
                                    <input type="passwords" name="passwords" id="passwords" class="form-control" placeholder="Password" />
                                </div>
                                <button type="submit" class="btn btn-defaultd">Login</button>
                            </form>
                            <a href="javascript:;" id="signUp">Sign Up</a>
                         </div>
                       <div class="signup_session">
                            <div class="head_popup">
                                <p>Sign Up</p>
                            </div>
                        	<form method="post" action="add-signup.php" onsubmit="return(validate());">
                            	<div class="form-group">
                                	<input type="text" name="name" id="name" class="form-control" placeholder="Name" required  />
                                </div>
                            	<div class="form-group">
                                	<input type="text" name="email" id="email" onBlur="checkAvailability()" required class="form-control" placeholder="Email Address" />
                                </div>
                            	<div class="form-group">
                                    <input type="text" id="password" name="password" class="form-control" placeholder="Password" required />
                                </div>
                            	<div class="form-group">
                                    <input type="text" id="phone" name="phone" class="form-control" placeholder="Phone Number" />
                                </div>
                            	<div class="form-group">
                                    <input type="text" id="location" name="location" class="form-control" placeholder="Location" />
                                    <input type="hidden"  value="0" id="active" name="active"/>
                                </div>
                                <button type="submit" class="btn btn-default">Register</button>
                            </form>
                            <a href="javascript:;" id="loginS">Already Member ?</a>
                       </div>
                   </div>
            </div>
        </div>
     <!-----End Login Section popUp starts------>  
     
        <!----- Share Button popUp starts------>   
        <div class="pop-bg-book" id="share">
        	<div class="container-popup">
            	<div class="container-inside-popup text-center">
                	<div class="social_share_new">
                    	<a href="#" style="background:url('images/facebook.png') no-repeat center center;"></a>
                        <a href="#" style="background:url('images/twitter.png') no-repeat center center;"></a>
                        <a href="#" style="background:url('images/insta.png') no-repeat center center;"></a>
                        <a href="#" style="background:url('images/google.png') no-repeat center center;"></a>
                    <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
       <!-----End Share Button popUp starts------>  
       
              <div class="load_here" id="load_here">
              </div>
               
       
<script type="text/javascript">
function loademe(loaddata){
    $.ajax({
        type: "GET",
		url: loaddata,
        data: { },
		success: function(data){
        $('#load_here').html(data);
		$(".main-container-over").hide();
        }
    });
	
 }
function usercheck()
{	
var result="";
if(($("#usernames").val())&&($("#passwords").val())){
jQuery.ajax({
url: "check_usern.php",
data:{uname:$("#usernames").val(),pword:$("#passwords").val()},
type: "POST",
async: false,
success:function(data){
	result = data; 
},
error:function (){
	var det = '<span style="color:#FFF"> Username and password does not match!.</span>';
	        $("#user-notexist-status").html(det);
            return false;
	}
});
if(result=='notexist'){
	var det = '<span style="color:#FFF"> Username and password does not match!.</span>';
	        $("#user-notexist-status").html(det);
            return false;
}else
{
	return true;
}
}
else
{
	        var det = '<span style="color:#FFF"> Please fill both the fields!.</span>';
	        $("#user-notexist-status").html(det);
            return false;
	
}
}
function checkAvailability() {
if($("#email").val()){	
jQuery.ajax({
url: "check_availability.php",
data:'username='+$("#email").val(),
type: "POST",
success:function(data){
	if(data=='exist'){
	var det = '<span style="color:#F00"> Unavailable.</span>';
	$("#email").html(det);
	$("#email").val(""); 
	$("#email").focus();
	}
	else
    {
	var det = '<span style="color:#060"> Available.</span>';
	$("#user-availability-status").html(det);
	}
	
	
},
error:function (){}
});
}}
 	$(document).ready(function(e) {
        $('.HotelListSlider').owlCarousel({
			loop:true,
			autoplay:true,
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			margin:0,
			nav:false,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:3
				},
				1000:{
					items:5
				}
			}
		});
		$('#signUp').click(function(){
			
		})
    });
function validate()
{
if($("#email").val()){	
jQuery.ajax({
url: "check_availability.php",
data:'username='+$("#email").val(),
type: "POST",
success:function(data){
	if(data=='exist'){
	alert( "Please provide your Email!" );
           $("#email").focus();
            return false;
	}
},
error:function (){}
});
}
else
{
	alert( "Please provide your Email!" );
           $("#email").focus();
            return false;
	
}
}

 </script>
    	
<?php 
	include_once('includes/footer.php');
?>