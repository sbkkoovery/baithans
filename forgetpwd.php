<?php 
@session_start();
include_once("class/common_class.php");
$objCommon		=	new common();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>Forget Password</title>

    <link href="admin/css/style.css" rel="stylesheet">
    <link href="admin/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">

<div class="container">
 <form class="form-signin" name="myForm" method="post" action="forgetmail.php" onsubmit="return(validate());">
        <div class="form-signin-heading text-center">
            <h1 class="sign-title">Password</h1>
            <img src="images/login-logo.png" alt=""/>
        </div>
        <div class="login-wrap">
			<?php echo $objCommon->displayMsg(); ?>
            <div class="modal-header">
            <h4 class="modal-title">Forgot Password ?</h4>
            </div>
            <div class="modal-body">
            <p>Enter your e-mail address below to reset your password.</p>
            <input type="email" id="email" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
            </div>
            <button type="submit" class="btn btn-primary" style="margin-left:70%" >Submit</button>
            </div>
                                      
        </div>
  </form>
</div>



<!-- Placed js at the end of the document so the pages load faster -->

<!-- Placed js at the end of the document so the pages load faster -->
<script src="admin/js/jquery-1.10.2.min.js"></script>
<script src="admin/js/bootstrap.min.js"></script>
<script src="admin/js/modernizr.min.js"></script>

</body>
</html>
<script type="text/javascript">
function validate()
{
if($("#email").val()){	
jQuery.ajax({
url: "check_availability.php",
data:'username='+$("#email").val(),
type: "POST",
success:function(data){
	if(data=='notexist'){
	alert( "Please use a registered email id!" );
	 $("#email").focus();
     return false;
	}
	
	
	
},
error:function (){}
});
}
else
{
	alert( "Please provide your Email!" );
           $("#email").focus();
            return false;
	
}
}
</script>
