<?php 
@session_start();
ob_start();
	include_once('includes/header.php');
	include_once('class/common_class.php');
	include_once('class/hotels.php');
	include_once('class/hotel_sliders.php');
	$hid	=	$_GET['fid'];
	if(isset($_SESSION['user'])){
	$username	=	$_SESSION['user']["userName"]; 
	}
	
	$objHotels			  	   =	new hotels();
	$objHotelsliders			 =	new hotel_sliders();
	$objCommon		 		   =	new common();
	$getHotelDetails	   =	$objHotels->listQuery("SELECT hotel.*,det.hd_created,det.hd_welcome_descr,det.hd_logo
												       FROM hotels AS hotel
													   LEFT JOIN hotel_details AS det ON hotel.h_id = det.h_id
													   WHERE hotel.h_id=".$hid);													    $getBGImages           =    $objHotelsliders->listQuery("SELECT sl.*,det.hd_created
												       FROM hotel_sliders sl
													   LEFT JOIN hotel_details AS det ON sl.h_id = det.h_id
													   WHERE sl.h_id=".$hid);
												   
?>
<link rel="stylesheet" href="admin/js/bootstrap-datepicker/css/datepicker.css" type="text/css"/>
<link rel="stylesheet" href="admin/css/bootstrap.css" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'>
<script src="js/hotel.js" type="text/javascript"></script>
<link href="css/hotel.css" rel="stylesheet" type="text/css" />
<style>
	body{
		overflow:hidden;
	}
</style>
<div class="main-container">
    <div class="slider-right">
        <div class="slider" id="slide">
            <ul class="slideme" >
                <?php if(count($getBGImages)>0){
					foreach($getBGImages as $bgimgs){ 
					$bgi = $objCommon->html2text($bgimgs['hs_img']);
					if($bgi){
					$bgdt = $objCommon->html2text($bgimgs['hd_created']);
					$bgimg = 'uploads/hotels/slider/'.$bgi; }?>
                <li><img src="<?=$bgimg?>" /></li>
                <?php  }}?>
            </ul>
        </div>
    </div>
    <div class="ovelay-container"></div>
    
    
    <div class="log-left">
        	<a href="index.php"><img src="images/logo.png" /></a>
        </div>
        <div id="menu-right" class="menu-right pull-right ">
        <?php if(isset($username)){?> 
        <p>Welcome <?php echo $username;?></p>
        <?php }else{ ?>
        <p>Welcome Guest</p>
        <?php } ?>
        </div>
        <?php if(count($getHotelDetails)>0){
			//print_r($getHotelDetails); die;
           foreach($getHotelDetails as $hotel){ 
		   $im = $objCommon->html2text($hotel['hd_logo']);
		   if($im){
		   $dt = $objCommon->html2text($hotel['hd_created']);
		   $lgo = 'uploads/hotels/logo/'.$im; }	
		   $hdd = $objCommon->html2text($hotel['h_id']);
		   ?>
    	<div class="lognd">
        	<div class="logo-hotels">
            	<img class="center-block" src="<?=$lgo?>" />
            </div>
            <div class="welcome text-center">
            	<h2><span>Welcome To</span><br/>Hotel <?php echo $objCommon->html2text($hotel['h_name']); ?></h2>
                <!--<p class="sub-tag">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>-->
           <p class="sub-tag"><?php echo $objCommon->html2text($hotel['hd_welcome_descr']); ?></p>
            </div>
        </div>
        
        <ul class="nav-bottm" id="nav-bottm">
        	<li onclick="loademe('ajax/facility.php');">Facilities</li>
            <li onclick="loademe('ajax/gallery.php?fid=<?php echo $hdd; ?>');">Gallery</li>
            <li id="no-nav" onclick="loademe('ajax/reservation.php?hid=<?php echo $hid; ?>');">Reservation</li>
            <li onclick="loademe('ajax/achievements.php');">Achievements</li>
            <li onclick="loademe('ajax/contact.php');">Contact Us</li>
        </ul>
        <div class="about-us" id="about-us">
        	<div class="load-contents" id="load-contents">
            	<div class="close-bounce">
                    <img src="images/close.png" width="22" />
                </div>
            	<div class="sk-spinner sk-spinner-wave inside" id="inside">
                  <div class="sk-rect1"></div>
                  <div class="sk-rect2"></div>
                  <div class="sk-rect3"></div>
                  <div class="sk-rect4"></div>
                  <div class="sk-rect5"></div>
                </div>
                <div class="logo-fixed">
                         	<img class="img-responsive" src="<?=$lgo?>" />
                </div>
                <div class="load_here" id="load_here">
                </div>
            </div>
        </div>
        <?php  }}?>
</div>
<script src="admin/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
			$('#slide').slideme({
				autoslide : true,
				loop : true,
				interval : 2000,
				speed : 1000,
				arrows: false,
				autoslideHoverStop : false,
				css3 : true,
				transition : 'fade',
				touch : true
				});
				$('#date1').datepicker({
                    format: "dd/mm/yyyy"
                }); 
			})
	</script>
<?php 
	include_once('includes/footer.php');
?>