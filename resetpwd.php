<?php 
@session_start();
include_once("class/common_class.php");
$objCommon		=	new common();
if (isset($_GET['email']) && isset($_GET['hash'])){
	$email =  $_GET['email']; 
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>Reset Password</title>

    <link href="admin/css/style.css" rel="stylesheet">
    <link href="admin/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">

<div class="container">
 <form class="form-signin" name="myForm" method="post" action="add-newpwd.php" onsubmit="return(validate());">
        <div class="form-signin-heading text-center">
            <h1 class="sign-title">Reset Password</h1>
            <img src="images/login-logo.png" alt=""/>
        </div>
        <div class="login-wrap">
			<?php echo $objCommon->displayMsg(); ?>
            <div class="modal-header">
            <h4 class="modal-title">reset Password</h4>
            </div>
              <div class="modal-body">
            <p>Enter your new password below.</p>
             <label>Password: </label>
             <input id="password" type="password" name="password" placeholder="New Password" required /><br/>
              <input type="hidden"  value="<?php echo $email;?>" id="email" name="email"/>
            </div>   
            <button type="submit" class="btn btn-primary" style="margin-left:50%" >Reset Password</button>
        </div>
  </form>
</div>



<!-- Placed js at the end of the document so the pages load faster -->

<!-- Placed js at the end of the document so the pages load faster -->
<script src="admin/js/jquery-1.10.2.min.js"></script>
<script src="admin/js/bootstrap.min.js"></script>
<script src="admin/js/modernizr.min.js"></script>

</body>
</html>
<script type="text/javascript">
function validate()
{
if($("#password").val())
{
}
else
{
	alert( "Please provide a new password!" );
           $("#password").focus();
            return false;
	
}
}
</script>
