<?php 
@session_start();
include_once("class/common_class.php");
$objCommon		=	new common();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>Login</title>

    <link href="admin/css/style.css" rel="stylesheet">
    <link href="admin/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">

<div class="container">
 <form class="form-signin" name="myForm" method="post" action="add-signup.php" onsubmit="return(validate());">
        <div class="form-signin-heading text-center">
            <h1 class="sign-title">Enter Details</h1>
            <img src="images/login-logo.png" alt=""/>
        </div>
        <div class="login-wrap">
			<?php echo $objCommon->displayMsg(); ?>
            <label>Name:</label>
            <input type="text" name="name" id="name" class=""  placeholder="" required ><br/>
            <label>Email: </label>
            <input name="email" type="text" id="email" placeholder="" onBlur="checkAvailability()" required><span id="user-availability-status" ></span><br/>
            <label>Password: </label>
	        <input id="password" type="text" name="password" placeholder="" required /><br/>
            <label>Phone: </label>
	        <input id="phone" type="text" name="phone"/></p>
            <label>Location: </label>
            <input id="location" type="text" name="location"/>
            <input type="hidden"  value="0" id="active" name="active"/>
            <button type="submit" class="btn btn-primary" style="margin-left:70%" >Register</button>
                                      
        </div>
  </form>
</div>



<!-- Placed js at the end of the document so the pages load faster -->

<!-- Placed js at the end of the document so the pages load faster -->
<script src="admin/js/jquery-1.10.2.min.js"></script>
<script src="admin/js/bootstrap.min.js"></script>
<script src="admin/js/modernizr.min.js"></script>

</body>
</html>
<script type="text/javascript">
function checkAvailability() {
if($("#email").val()){	
jQuery.ajax({
url: "check_availability.php",
data:'username='+$("#email").val(),
type: "POST",
success:function(data){
	if(data=='exist'){
	var det = '<span style="color:#F00"> Unavailable.</span>';
	$("#user-availability-status").html(det);
	$("#email").val("");
	$("#email").focus();
	}
	else
    {
	var det = '<span style="color:#060"> Available.</span>';
	$("#user-availability-status").html(det);
	}
	
	
},
error:function (){}
});
}}
function validate()
{
if($("#email").val()){	
jQuery.ajax({
url: "check_availability.php",
data:'username='+$("#email").val(),
type: "POST",
success:function(data){
	if(data=='exist'){
	alert( "Please provide your Email!" );
           $("#email").focus();
            return false;
	}
	
	
	
},
error:function (){}
});
}
else
{
	alert( "Please provide your Email!" );
           $("#email").focus();
            return false;
	
}
}
</script>
