<?php 
@session_start();
include_once('includes/header.php');
include_once("class/hotel_details.php");
include_once("class/common_class.php");
include_once("class/hotels.php");
include_once("class/hotel_rooms.php");
include_once("class/bookdetails.php");
$objCommon                   =	new common();
$objHotels			  	   =	new hotels();
$objBDetails                 =	new bookdetails();
$objHotelrooms			   =	new hotel_rooms();
$objHotelsliders			 =	new hotel_details();
if(isset($_SESSION[user][userId])){
$uname = $_SESSION[user][userName];
$login_session_duration = 600;
$logtime = $_SESSION[user][loggedin_time]; 
$current_time = time();
$diftime = ($logtime-$current_time);
$uid = $_SESSION[user][userId];}
else{
$uname = "Guest";}
if(isset($_GET['confirm'])){
	$confirm = $_GET['confirm'];}
if($confirm=='yes'){
$_POST['u_id']=$uid;
$_POST['b_id']='';
$_POST['fname']=$_POST['fname'];
$_POST['lname']=$_POST['lname'];
$_POST['email']=$_POST['email1'];
$_POST['tripwork']=$_POST['optionsRadios1'];
$_POST['tripleiss']=$_POST['optionsRadios2'];
$_POST['smoke']=$_POST['smoke'];
$_POST['srequest']=$_POST['specialr'];
$_POST['highfloor']=$_POST['highfloor'];
$_POST['groundfloor']=$_POST['lowfloor'];
$_POST['freepark']=$_POST['parking']; 
//print_r($_POST); die;
$objBDetails->insert($_POST);
$bId = mysql_insert_id();
}
if(isset($_GET['date1']) && isset($_GET['date2']) && isset($_GET['hid']) && isset($_GET['roomid'])){
	$date1	=	$objCommon->esc($_GET['date1']);
	$date2	=	 $objCommon->esc($_GET['date2']);
	$hid	=	 $objCommon->esc($_GET['hid']);
	$roomid	=	 $objCommon->esc($_GET['roomid']); 

$getHotelDetails	   =	$objHotels->listQuery("SELECT hotel.*,det.*
												       FROM hotels AS hotel
													   LEFT JOIN hotel_details AS det ON hotel.h_id = det.h_id
													   WHERE hotel.h_id=".$hid);
$getRooms	          =	$objHotelrooms->listQuery("SELECT r.*
												       FROM hotel_rooms AS r
													   WHERE r.room_id=".$roomid);
}
?><head>
     <link rel="stylesheet" type="text/css" href="css/login_style.css">
     <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
     <script type="text/javascript" src="js/login_effect.js"></script>
</head>


<style>
	body{
		background:url('images/bginside.jpg') repeat-x center top;
	}
</style>
<div class="main-bar">
	<div class="container">
    	<div class="row">
          <?php if(count($getHotelDetails)>0){
		   foreach($getHotelDetails as $hotel){ 
		   $im = $objCommon->html2text($hotel['hd_logo']);
		   $hplace = $objCommon->html2text($hotel['hd_place']);
		   if($im){
		   $dt = $objCommon->html2text($hotel['hd_created']);
		   $lgo = 'uploads/hotels/logo/'.$im; }	
		   $hdd = $objCommon->html2text($hotel['h_id']);
		   ?>
    	<div class="col-sm-1 lessed-padding-r">
        	<a href="hotels.php?fid=<?php echo $hid;?>"><img class="img-responsive log-inside" src="<?=$lgo?>" width="60" /></a>
        </div>
        <div class="col-sm-5 lessed-padding-l">
        	<p class="hotel-name-s"><?php echo $objCommon->html2text($hotel['h_name']); ?><br/><span class="sml-text">Appartments </span></p>
        </div>
        <div class="col-sm-6">
        	<div class="user-info pull-right">
            	<div class="info-sec ">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="img-user">
                                <img class="img-responsive" src="images/img2.jpg" />
                            </div>
                        </div>
                        <div class="col-sm-7">
                        	<div class="info-name">
                            	<p><?php echo $uname;?></p>
                                <p class="arng-i"><small><i class="fa fa-thumb-tack"></i>Booking List</small><i class="fa fa-chevron-down"></i></p>
                            </div>
                        </div>
                    </div>
                <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
                <div class="clearfix"></div>
        </div>
         <?php  }}?>
        </div>
    </div>
</div>
<div class="container">
	<div class="top-sections">
        <ul>
          <?php if($confirm=='yes'){ ?>
            <li id="selectroom">1 . Select Room</li>
            <li id="enterdetails">2 . Enter Your Details</li>
            <li id="cfmr" class="actived-selct">3 . Confirm Your Reservation</li>
            <li id="rsvd">4 . Your Room Reserved</li>
           <?php }else{?>
            <li id="selectroom" class="actived-selct">1 . Select Room</li>
            <li id="enterdetails">2 . Enter Your Details</li>
            <li id="cfmr">3 . Confirm Your Reservation</li>
            <li id="rsvd">4 . Your Room Reserved</li>
        <?php }?>
        </ul>
    </div>
	<div class="row" id="row1">
    	<div class="col-sm-3">
        	<div class="side-box-l text-left">
            	<p class="head-search">Your preference</p>
                <div class="info-booking text-left">
                	<ul>
                    	<li><i class="fa fa-building"></i><?=$hplace?></li>
                         <?PHP $diff= ceil(abs($date1 - $date2)/86400); ?>
                        <li><i class="fa fa-calendar"></i><?=$diff?>-night stay</li>
                        <li><i class="fa fa-users"></i>2 Adult</li>
                    </ul>
                </div>
            </div>
            <div class="side-box-l text-left">
            	<p class="head-search">Property Highlights</p>
                <div class="info-booking text-left">
                	<p>Perfect for <?=$diff?> days stay !</p>
                    <p>Top Location : Highly Rated By Users</p>
                	<ul>
                    	<li><i class="fa fa-bed"></i>Luxury Rooms</li>
                        <li><i class="fa fa-wifi"></i>Free Wifi2 </li>
                        <li><i class="fa fa-car"></i>Free Parking</li>
                    </ul>
                    <div class="highlights-on">
                    	<p><b>Kitchenette with:</b></p>
                    	<ul>
                        	<li>Cleaning products</li>
                            <li>Kitchenware</li>
                            <li>Refrigerator</li>
                            <li>Stovetop</li>
                        </ul>
                    </div>
                    <div class="highlights-on">
                    	<p><b>Popular Facilities:</b></p>
                    	<ul>
                        	<li><i class="fa fa-user"></i>Family Rooms</li>
                            <li><i class="fa fa-ban"></i>No Smoking Rooms</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="side-box-l text-left">
            	<p class="head-search">Check Location</p>
                <div class="info-booking text-left">
                	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3606.342791982557!2d55.37829139999999!3d25.326275799999987!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f5b9574d9095d%3A0xd5ea13f9ee5482ad!2sDesign+Days!5e0!3m2!1sen!2sae!4v1435734580006" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="col-sm-9">
        	<div class="type-room" id="type-room">
            	<table class="table table-bordered">
                <thead>
                	<th>Accommodation Type</th>
                    <th>Max</th>
                    <th>Price for <?=$diff?> nights</th>
                    <th>Conditions</th>
                    <th>Quantity</th>
                    <th>Reservation</th>
                </thead>
                <tbody>
                	
            <?php if(count($getRooms)>0){
			      foreach($getRooms as $rooms){ ?>
                    	<tr>
                        <td><?=$objCommon->html2text($rooms['room_type'])?></td>
                        <td>2 adults, 1 child</td>
                        <?php $priceone =$objCommon->html2text($rooms['price']);
						      $totprice =($diff)*($priceone);
						  ?>
                        <td><?=$totprice?> </td>
                        <td>
                        	<ul class="tale-li">
                            	<li>Special conditions, pay when you stay</li>
                                <li> Buffet breakfast included</li>
                            </ul>
                        </td>
                        <td>
                        	<form>
                              <div class="form-group">
                              	<select class="form-control">
                                	<option>1</option>
                                </select>
                              </div>
                          </form>
                        </td>
                        <td rowspan="3" style="vertical-align:middle;">
                        	<div>
                                <p class="appartment-d">1 Appartment for</p>
                                <p class="text-center"><b>AED <?=$totprice?></b></p>
                               <div class="reserve-btn"> 
                               <?php if($confirm=='yes'){?>
								   <a href="#" onclick="loadstep('savebookings.php?hid=<?php echo $hid;?>&date1=<?php echo $date1;?>&date2=<?php echo $date2;?>&roomid=<?php echo $roomid;?>&bid=<?php echo $bId;?>');">Confirm</a>
							  <?php } else{
								  if(isset($_SESSION[user][userId])){?>
                                <a href="#" onclick="loadstep('ajax/step2.php?hid=<?php echo $hid;?>&date1=<?php echo $date1;?>&date2=<?php echo $date2;?>&roomid=<?php echo $roomid;?>');">Reserve</a>
                               <?php }else{?>
                               <input type="button"  class="login" id="" value="Reserve">
                                <?php }}?>
                               </div>
                              
                             
                              </div>
                        </td>
						</tr>
                         </tbody>
                </table>
                    <?php  }}?>
                    
                     <div class="pop-bg-book" id="login">
        	<div class="container-popup">
            	<div class="container-inside-popup text-center">
                        <div class="login_session">
                           <span id="user-notexist-status" ></span>
                            <div class="head_popup">
                                <p>Login</p>
                            </div>
                        	<form class="form-signin" method="post" action="dologin.php" onsubmit="return(usercheck());">
                                   	<div class="form-group">
                                	<input type="text" name="usernames" id="usernames" class="form-control" placeholder="Username or Email Address" />
                                </div>
                            	<div class="form-group">
                                    <input type="passwords" name="passwords" id="passwords" class="form-control" placeholder="Password" />
                                </div>
                                <button type="submit" class="btn btn-defaultd">Login</button>
                            </form>
                            <a href="javascript:;" id="signUp">Sign Up</a>
                         </div>
                       <div class="signup_session">
                            <div class="head_popup">
                                <p>Sign Up</p>
                            </div>
                        	<form method="post" action="add-signups.php" onsubmit="return(validate());">
                            	<div class="form-group">
                                	<input type="text" name="name" id="name" class="form-control" placeholder="Name" required  />
                                </div>
                            	<div class="form-group">
                                	<input type="text" name="email" id="email" onBlur="checkAvailability()" required class="form-control" placeholder="Email Address" />
                                </div>
                            	<div class="form-group">
                                    <input type="text" id="password" name="password" class="form-control" placeholder="Password" required />
                                </div>
                            	<div class="form-group">
                                    <input type="text" id="phone" name="phone" class="form-control" placeholder="Phone Number" />
                                </div>
                            	<div class="form-group">
                                    <input type="text" id="location" name="location" class="form-control" placeholder="Location" />
                                    <input type="hidden"  value="0" id="active" name="active"/>
                                </div>
                                <button type="submit" class="btn btn-default">Register</button>
                            </form>
                            <a href="javascript:;" id="loginS">Already Member ?</a>
                       </div>
                   </div>
            </div>
        </div>
     <!-----End Login Section popUp starts------> 
                    
                    
                    
                    
                   <!-- <tr>
                    	<td>Superior studio- Twin Beds</td>
                        <td>2 adults, 1 child</td>
                        <td>AED 498 </td>
                        <td>
                        	<ul class="tale-li">
                            	<li>Special conditions, pay when you stay</li>
                                <li> Buffet breakfast included</li>
                            </ul>
                        </td>
                        <td>
                        	<form>
                              <div class="form-group">
                              	<select class="form-control">
                                	<option>1</option>
                                	<option>2</option>
                                </select>
                              </div>
                          </form>
                        </td>
                        <tr>
                    	<td>Superior studio- Twin Beds</td>
                        <td>2 adults, 1 child</td>
                        <td>AED 498 </td>
                        <td>
                        	<ul class="tale-li">
                            	<li>Special conditions, pay when you stay</li>
                                <li> Buffet breakfast included</li>
                            </ul>
                        </td>
                        <td>
                        	<form>
                              <div class="form-group">
                              	<select class="form-control">
                                	<option>1</option>
                                	<option>2</option>
                                </select>
                              </div>
                          </form>
                        </td>
                    </tr>
                    </tr>-->
                
                <center>
	            <div id = "loginform" style="margin-right:15%;">
                <form method = "post" action = "dologin.php">
                <input type = "image" id = "close_login" src = "images/close.png">
                <input type = "text" id = "login" style="margin-top:10%;" placeholder = "Email Id" name = "uid">
                <input type = "password" id = "password" name = "upass" placeholder = "Password">
                <input type = "submit" id = "dologin" value = "Login">
                <div> <?php echo $objCommon->displayMsg(); ?></div>
                </form>
               </div>
               </center>
            </div>
		</div>
    </div>
</div>
<div class="mini-footer">
	<div class="container">
    	<div class="copyright">
        	<p><small>&copy; 2015 Baithans</small></p>
        </div>
    </div>
</div>
<script type="text/javascript">
function usercheck()
{	
var result="";
if(($("#usernames").val())&&($("#passwords").val())){
jQuery.ajax({
url: "check_usern.php",
data:{uname:$("#usernames").val(),pword:$("#passwords").val()},
type: "POST",
async: false,
success:function(data){
	result = data; 
},
error:function (){
	var det = '<span style="color:#FFF"> Username and password does not match!.</span>';
	        $("#user-notexist-status").html(det);
            return false;
	}
});
if(result=='notexist'){
	var det = '<span style="color:#FFF"> Username and password does not match!.</span>';
	        $("#user-notexist-status").html(det);
            return false;
}else
{
	return true;
}
}
else
{
	        var det = '<span style="color:#FFF"> Please fill both the fields!.</span>';
	        $("#user-notexist-status").html(det);
            return false;
	
}
}
function validate()
{
if($("#email").val()){	
jQuery.ajax({
url: "check_availability.php",
data:'username='+$("#email").val(),
type: "POST",
success:function(data){
	if(data=='exist'){
	alert( "Please provide your Email!" );
           $("#email").focus();
            return false;
	}
},
error:function (){}
});
}
else
{
	alert( "Please provide your Email!" );
           $("#email").focus();
            return false;
	
}
}
function checkAvailability() {
if($("#email").val()){	
jQuery.ajax({
url: "check_availability.php",
data:'username='+$("#email").val(),
type: "POST",
success:function(data){
	if(data=='exist'){
	var det = '<span style="color:#F00"> Unavailable.</span>';
	$("#email").html(det);
	$("#email").val(""); 
	$("#email").focus();
	}
	else
    {
	var det = '<span style="color:#060"> Available.</span>';
	$("#user-availability-status").html(det);
	}
	
	
},
error:function (){}
});
}}
 	$(document).ready(function(e) {
        $('.HotelListSlider').owlCarousel({
			loop:true,
			autoplay:true,
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			margin:0,
			nav:false,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:3
				},
				1000:{
					items:5
				}
			}
		});
		$('#signUp').click(function(){
			
		})
    });
</script>
<?php 
	include_once('includes/footer.php');
?>