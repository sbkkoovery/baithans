<?php
@session_start();
ob_start();
$hid	=	$_GET['hid'];
include_once("../class/common_class.php");
include_once("../class/pagination-class.php");
include_once("../class/widgets.php");
include_once("../class/widgets_assign.php");
include_once("../class/hotels.php");
include_once("../class/hotel_rooms.php");
include_once("../class/hotel_sliders.php");
if(isset($_SESSION[user][userId]))
{
   $uid = $_SESSION[user][userId];
}

$objCommon                   =	new common();
$objHotels			  	   =	new hotels();
$objHotelrooms			  	   =	new hotel_rooms();
$objHotelsliders			 =	new hotel_sliders();
$objWidgets     		  	  =	new widgets();
$objWidgets_assign		   =	new widgets_assign();

$nId			  			 =	$objCommon->esc($_GET['nId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_GET['search']);

if($nId){
		$getRowDetails	   =	$objWidgets->getRow("widget_id=".$nId);
}
if($dId){
		$objWidgets->delete("widget_id=".$dId);
		$objCommon->addMsg("Selected item has been deleted successfully.",1);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
}

$sql						 .= "SELECT * FROM reason_widget WHERE 1 ";
if($search){
	$sql					.= " AND (widget_title LIKE '%".$search."%' OR widget_id LIKE '%".$search."%')";	
}
$sql						 .= " ORDER by widget_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
//$contentList				 =	$objPages->listQuery($paginationQuery);
$getHotelDetails	   =	$objHotels->listQuery("SELECT hotel.*,det.*
												       FROM hotels AS hotel
													   LEFT JOIN hotel_details AS det ON hotel.h_id = det.h_id
													   WHERE hotel.h_id=".$hid);
$getBGImages           =    $objHotelsliders->listQuery("SELECT sl.*,det.hd_created
												       FROM hotel_sliders sl
													   LEFT JOIN hotel_details AS det ON sl.h_id = det.h_id
													   WHERE sl.h_id=".$hid);
$getHotelrooms	   =	$objHotelrooms->listQuery("SELECT hr.*
												       FROM hotel_rooms AS hr
													   WHERE hr.h_id=".$hid);
if(count($getHotelrooms)>0){
		   foreach($getHotelrooms as $hotelrms){
			 $hotelrmcnt = $objCommon->html2text($hotelrms['room_cnt']);
		   }
}

?>
<link rel="stylesheet" href="admin/js/bootstrap-datepicker/css/datepicker.css">
<link rel="stylesheet" href="admin/css/bootstrap.css">
<div class="map-sec" id="map-sec">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3606.342791982557!2d55.37829139999999!3d25.326275799999987!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f5b9574d9095d%3A0xd5ea13f9ee5482ad!2sDesign+Days!5e0!3m2!1sen!2sae!4v1435728771066" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<?php if(count($getHotelDetails)>0){
		   foreach($getHotelDetails as $hotel){ 
		   $hotel_name = $objCommon->html2text($hotel['h_name']);
		   $hotel_place = $objCommon->html2text($hotel['hd_place']);
		   $hotel_highlights = $objCommon->html2text($hotel['hd_highlights']);
		   $lgo = $objCommon->html2text($hotel['hd_logo']);
		   if($lgo){
		   		   $hotel_logo = 'uploads/hotels/logo/'.$lgo;
				   }	
		   ?>

<div class=" container-fluid">
	<div class="reserve">
        <div class="row">
            <div class="col-sm-1">
                <div class="nav-left">
                    <ul>
                        <li onclick="loademe('ajax/facility.php');">Facilities</li>
                        <li onclick="loademe('ajax/gallery.php?fid=<?php echo $hid; ?>');">Gallery</li>
                        <li id="no-nav-inside" onclick="loademe('ajax/reservation.php?hid=<?php echo $hid; ?>');">Reservation</li>
                         <li onclick="loademe('ajax/achievements.php');">Achievements</li>
                        <li onclick="loademe('ajax/contact.php');">Contact Us</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6">
               <div class="slider-inside">
               <?php if(count($getBGImages)>0){
					foreach($getBGImages as $bgimgs){ 
					$bgi = $objCommon->html2text($bgimgs['hs_img']);
					if($bgi){
					$bgdt = $objCommon->html2text($bgimgs['hd_created']);
					$bgimg = 'uploads/hotels/slider/'.$bgi; }?>
                    <div class="img-sec">
                        <img class="img-responsive" src="<?=$bgimg?>" />
                    </div>
                <?php  }}?>
               		
                   <!-- <div class="img-sec">
                        <img class="img-responsive" src="images/bg5.jpg" />
                    </div>
                    <div class="img-sec">
                        <img class="img-responsive" src="images/bg7.jpg" />
                    </div>-->
               </div>
               <div class="head-down-slide">
               		<div class="row">
                    	<div class="col-sm-8">
                            <h4><?=$hotel_name?><!--<img src="images/star.png" />--></h4>
                            <p><i class="fa fa-map-marker"></i><?=$hotel_place?></p>
                        </div>
                        <div class="col-sm-4">
                        	<div class="reserveme text-center">
                            	<a href="http://maps.google.com/?q=<?=$hotel_name?> <?=$hotel_place?>;" target="_blank" class="pull-right">View On Map</a>
                            </div>
                        </div>
                    </div>
               </div>
               <div class="amminities">
               		<div class="row">
                    	<div class="col-sm-4">
                        	<div class="box-specs">
                            	<div class="spec-head">
                                	<p>Facilities</p>
                                </div>
                                <div class="specs-des">
                                	<ul>
                                        <li><img src="images/bedspace.png" /> Bedrooms : 3</li>
                                       <li><img src="images/bathroom.png" /> Bathroom : 2</li>
                                       <li><img src="images/parking.png" /> Parking : 1</li>
                                    </ul>
                                    <p class="kitch"><b>Kitchenette with:</b></p>
                                    <div class="small-spec">
                                    	<ul>
                                        	<li>Microwave</li>
                                            <li>Cleaning products</li>
                                            <li>Kitchenware</li>
                                            <li>Refrigerator</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                       <!-- Widget section-->
                        
                        <div class="col-sm-8">
                        	<div class="choose">
                            	<p>Reasons Why You Choose Hala Inn</p>
                            </div>
                            <div class="segments">
                            	<div class="row">
                                
                                <?php 
                                    /*if(count($contentList)>0){
                                    $i=1;
									$count_no = 0;
                                    foreach($contentList as $list){$count_no++;?>
                                
                                	<div class="col-sm-4">
                                    	<div class="rounded-sec">
                                        	<img class="center-block" src="widgets/<?php echo $objCommon->html2text($list['widget_img']); ?>" />
                                            <div class="desc-fac">
                                                <p><?php echo $objCommon->html2text($list['widget_title']); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php
									}}*/
									?>
                                    
                                    <?php
                                   
                                    $checkWid				 =	$objWidgets_assign->listQuery("select * from tbl_hotel_widget	WHERE hotel_id='".$hid."'");

if(count($checkWid)>0)
{
	$i=1;
	$count_no = 0;
	foreach($checkWid as $list){$count_no++;
	
	$checkWid_real				 =	$objWidgets->listQuery("select * from reason_widget WHERE widget_id='".$objCommon->html2text($list['widget_id'])."'");
	
foreach($checkWid_real as $list_real){
?>

<div class="col-sm-4">
                                    	<div class="rounded-sec">
                                        	<img class="center-block" src="widgets/<?php echo $objCommon->html2text($list_real['widget_img']); ?>" />
                                            <div class="desc-fac">
                                                <p><?php echo $objCommon->html2text($list_real['widget_title']); ?></p>
                                            </div>
                                        </div>
                                    </div>

<?php
}
	}
}
else
{
	echo "No assigned widgets";
?>
<?php
}
?>

                                    
                                    
                                    
                                    
                                    
                                </div>
                             </div>
                        </div>
                        
                        <!-- Widget section-->
                        
                    </div>
               </div>
            </div>
            <div class="col-sm-5 inc-padding">
               <div class="specs-inside">
               		<p>Highlights</p>
               </div>
               <div class="spec-desc">
               <p><?=$hotel_highlights?></p>
               		<!--<p>In Ajman, the Hala Inn Hotel Apartments have a restaurant. The self-catering accommodations all feature Wi-Fi. Guests can shop in the center of Ajman, which is 293 m from the Hala Inn.</p>
                    <p>The apartments all have a flat-screen TV, air conditioning and a minibar. The private bathrooms also include a shower. Some of the properties have a living room and a kitchennette.</p>
                    <p>At Hala Inn Hotel Apartments, there is a sauna and a 24-hour front desk. Other facilities like a tour desk, luggage storage and an ironing service are available.</p>
                    <p>Sharjah Airport is 25.7 km from Hala Inn. Dubai International Airport is 40 minutes away by car. Free private parking is available on site.</p>-->
               </div>
               <div class="check-availability">
               		<div class="head-check">
                    	<p>When would you like to stay at <?=$hotel_name?>  Hotel Apartments?</p> 
                    </div>
               		<div class="booking-sec  booking-sec-fields">
                    	<div class="row">
                        	<div class="col-sm-5">
                            	<div class="form-group">
                                    <label>Check In</label>
                                    <div class="input-group">
                                       <input type="text" id="date1" class="form-control" />
                                       <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                              </div>
                            </div>
                            <div class="col-sm-5">
                            	<div class="form-group">
                                    <label>Check Out</label>
                                    <div class="input-group">
                                       <input type="text" id="date2" class="form-control" />
                                       <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                              </div>
                            </div>
                            <div class="col-sm-2">
                            	<div class="submit checking">
                                	<button class="btn btn-default" id="slide-result" onclick="checkrooms(<?=$hid?>)">Check</button>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
               <div class="availablity-slide">
                      
               </div>
              <!-- <div class="acordian">
                   <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              Collapsible Group Item #1
                            </a>
                          </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              Collapsible Group Item #2
                            </a>
                          </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              Collapsible Group Item #3
                            </a>
                          </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                          </div>
                        </div>
                      </div>
                    </div>
                </div>-->
             </div>
        </div>
    </div>
</div>
 <?php  }}?>
 
<script src="js/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="js/hotel.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.matchHeight-min.js" type="text/javascript"></script>
<script src="js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#content-area").perfectScrollbar();
		$(function() {
			$('.rounded-sec').matchHeight();
		});
		$('#date1').datepicker({
                    format: "yyyy-mm-dd"
                }); 
		$('#date2').datepicker({
                   format: "yyyy-mm-dd"
               });  
	})
</script>
<script type="text/javascript">
function checkrooms(id){
var todayDate = new Date();
var date1 =new Date($("#date1").val());
var date2 =new Date($("#date2").val());
if(($("#date1").val()>$("#date2").val())){
alert("Please enter valid dates");
 $('.availablity-slide').html("");
}
else if($("#date1").val()==$("#date2").val()){
	alert("Please enter valid dates");
	 $('.availablity-slide').html("");
}
else if($("#date1").val()&&$("#date2").val()){	
if(todayDate > date1){
	alert("Please enter valid dates");
	  $('.availablity-slide').html("");
}
else
	jQuery.ajax({
url: "check_rooms.php",
data:'date1='+$("#date1").val()+'&date2='+$("#date2").val()+'&hid='+id,
type: "POST",
success:function(data){
	//alert(data);
	$('.availablity-slide').html(data);
},
error:function (){
	}
});
}}
</script>