<?php
@session_start();
ob_start();
include_once("../class/common_class.php");
include_once("../class/pagination-class.php");
$objCommon	=	new common();

include_once("../class/pages.php");
$objPages			  	   =	new pages();
$objCommon		 		   =	new common();

$nId			  			 =	$objCommon->esc($_GET['nId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_GET['search']);
if($nId){
		$getRowDetails	   =	$objPages->getRow("page_id=".$nId);
}
if($dId){
		$objPages->delete("page_id=".$dId);
		$objCommon->addMsg("Selected item has been deleted successfully.",1);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
}

$sql						 .= "SELECT * FROM 	pages WHERE page_alias='history' ";
if($search){
	$sql					.= " AND (page_title LIKE '%".$search."%' OR page_id LIKE '%".$search."%')";	
}
$sql						 .= " ORDER by page_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objPages->listQuery($paginationQuery);

?>
<style>
.ajax-loaded{
	padding:30px;
}
.img sec{
	width:100%;
	height:auto;
}
.img sec img{
	width:100%;
	height:auto;
}
.head-loaded{
	padding:30px;
}
</style>
<div class="top-sec-load">
    <span class="top-bar" id="toggle-down"> </span>
    <span class="heading-1">History</span>
</div>
<?php
  if(count($contentList)>0){
                                    $i=1;
                                    foreach($contentList as $list){
										echo $objCommon->html2text($list['page_des']);
									}
  }
?>
<script src="js/main.js" type="text/javascript"></script>
