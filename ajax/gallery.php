<?php 
ob_start();
include_once("../class/common_class.php");
include_once("../class/hotels.php");
include_once("../class/hotel_sliders.php");
    $hid	=	$_GET['fid']; 
    $objHotels			  	   =	new hotels();
	$objHotelsliders			 =	new hotel_sliders();
	$objCommon		 		   =	new common();
	$getBGImages           =    $objHotelsliders->listQuery("SELECT sl.*,det.hd_created
												       FROM hotel_sliders sl
													   LEFT JOIN hotel_details AS det ON sl.h_id = det.h_id
													   WHERE sl.h_id=".$hid);
?>


<style>
	.item{
		width:100%;
		height:300px;
		overflow:hidden;
	}
	.item img{
		width:100%;
		min-height:100%;
	}
	.owl-carousel{
		margin-top:50px;
	}
	.owl-dots{
		margin-top:20px;
	}

</style>
<div class="main-head-gallery">
	<p>GALLERY</p>
</div>
<div class="main-container-gallery">
	<div class="overlay-left"></div>
    <div class="overlay-right"></div>
	<div class="owl-carousel">
     <?php if(count($getBGImages)>0){
					foreach($getBGImages as $bgimgs){ 
					$bgi = $objCommon->html2text($bgimgs['hs_img']);
					if($bgi){
					$bgdt = $objCommon->html2text($bgimgs['hd_created']);
					$bgimg = 'uploads/hotels/slider/'.$bgi; }?>
    	<div class="item"><a href="<?=$bgimg?>" class="html5lightbox"><img src="<?=$bgimg?>" /></a></div>
    	<!--<div class="item"><a href="images/bg1.jpg" class="html5lightbox"><img src="images/bg1.jpg" /></a></div>
        <div class="item"><img src="images/bg2.jpg" /></div>
        <div class="item"><img src="images/bg3.jpg" /></div>
        <div class="item"><img src="images/bg4.jpg" /></div>
        <div class="item"><img src="images/bg5.jpg" /></div>
        <div class="item"><img src="images/bg3.jpg" /></div>
        <div class="item"><img src="images/bg7.jpg" /></div>-->
        <?php  }}?>
    </div>
</div>
<script src="js/owl.carousel.min.js" type="text/javascript"></script>
<script src="js/hotel.js" type="text/javascript"></script>
<script src="js/html5lightbox.js" type="text/javascript"></script>
<link href="css/hotel.css" rel="stylesheet" type="text/css" />