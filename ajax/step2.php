<?php 
@session_start();
include_once("../class/hotel_details.php");
include_once("../class/common_class.php");
include_once("../class/hotels.php");
include_once("../class/hotel_rooms.php");
$objCommon                   =	new common();
$objHotels			  	   =	new hotels();
$objHotelrooms			   =	new hotel_rooms();
$objHotelsliders			 =	new hotel_details();
$username='';
$email='';
if(isset($_SESSION['user'])){
	$username	=	$_SESSION['user']["userName"]; 
	$email	=	   $_SESSION['user']["email"]; 
	}
//$hid	=	 $objCommon->esc($_GET['hid']);
//$date1	=	 $objCommon->esc($_GET['date1']);
//$date2	=	 $objCommon->esc($_GET['date2']);
//$roomid	=	 $objCommon->esc($_GET['roomid']); die;
if (isset($_GET['date1']) && isset($_GET['date2']) && isset($_GET['hid']) && isset($_GET['roomid'])){
	$date1	=	$objCommon->esc($_GET['date1']);
	$date2	=	 $objCommon->esc($_GET['date2']); 
	$dtime1 = date("m/d/Y",$date1);
	$dtime2 = date("m/d/Y",$date2);
	$hid	=	 $objCommon->esc($_GET['hid']);
	$roomid	=	 $objCommon->esc($_GET['roomid']);
	$getHotelDetails	   =	$objHotels->listQuery("SELECT hotel.*,det.hd_created,det.hd_place,det.hd_home
												       FROM hotels AS hotel
													   LEFT JOIN hotel_details AS det ON hotel.h_id = det.h_id
													   WHERE hotel.h_id=".$hid);

}
?>
 <?php if(count($getHotelDetails)>0){
			//print_r($getHotelDetails); die;
           foreach($getHotelDetails as $hotel){ 
		   $im = $objCommon->html2text($hotel['hd_home']);
		   if($im){
		   $dt = $objCommon->html2text($hotel['hd_created']);
		   $lgo = 'uploads/hotels/home_image/'.$im; }	
		   $hdd = $objCommon->html2text($hotel['h_id']);
		   ?>
<div class="hotel-img">
    <div class="row">
        <div class="col-sm-4">
            <img class="img-responsive" src="<?=$lgo?>">
        </div>
           <div class="col-sm-8">
        	<div class="hotel-head-sec">
            	<p><b><?php echo $objCommon->html2text($hotel['h_name']); ?> Apartments</b></p>
                <p class="map-sec-text"><i class="fa fa-map-marker"></i><?php echo $objCommon->html2text($hotel['hd_place']); ?> , United Arab Emirates </p>
            </div>
            <div class="stay-details">
            <?PHP $diff= ceil(abs($date1 - $date2)/86400); ?>
            	<p><?=$diff?>-night stay. 2 adults</p>
            </div>
            <div class="dates-stay">
            <?php $checkindate  = date("M jS, Y", strtotime($dtime1));
			      $checkoutdate = date("M jS, Y", strtotime($dtime2));
			 ?>
            	<p><b>Check In :</b>  <?=$checkindate?> from 14:00</p>
                <p><b>Check Out :</b> <?=$checkoutdate?> untill 13:00</p>
            </div>
        </div>
      </div>
</div>
 <?php  }}?>
<div class="details-form">
	<div class="head-form">
    	<p>Enter Your Details</p>
    </div>
    <div class="form-sections">
    	<form class="" method="post" action="booking.php?hid=<?=$hid?>&date1=<?=$date1?>&date2=<?=$date2?>&roomid=<?=$roomid?>&confirm=yes" onsubmit="return(validateemail());">
        	<div class="row">
            	<div class="col-sm-6">
                  <div class="form-group">
                        <label>First Name</label>
                        <input type="text" name="fname" class="form-control" value="<?=$username?>" placeholder="First Name" required>
                  </div>
                  </div>
                  <div class="col-sm-6">
                  <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" name="lname" class="form-control" placeholder="Last Name">
                  </div>
                  </div>
                  </div>
                  <div class="row">
            	<div class="col-sm-6">
                  <div class="form-group">
                        <label>Email Adresss</label>
                        <input type="email" name="email1" id="email1" value="<?=$email?>" class="form-control" placeholder="Email Adresss"  required>
                  </div>
                  </div>
                  <div class="col-sm-6">
                  <div class="form-group">
                        <label>Confirm Your Email Adresss</label>
                        <input type="email" id="email2" name="email2" value="<?=$email?>" class="form-control" placeholder="Confirm Your Email Adresss" onblur="verifyemail()"  required>
                  </div>
                  </div>
                  <div class="col-sm-12">
                  	<div class="radio">
                      <label>
                        <input type="checkbox" name="optionsRadios1" id="optionsRadios1" value="option1" >
                        Purpose of my trip is Leisure
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="checkbox" name="optionsRadios2" id="optionsRadios2" value="option2">
                        Purpose of my trip is Work
                      </label>
                    </div>
                   </div>
                   <div class="col-sm-6">
                   		<label>Do You Smoke?</label>
                  		<select class="form-control margin-on" name="smoke">
                        	<option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                   </div>
                   <div class="col-sm-12">
                  <div class="form-group">
                        <label>Special Requests</label>
                        <p class="help-p"><i>Please write your requests in English or Arabic. <br/>
                                Special requests cannot be guaranteed, but the property will do its best to meet your needs.</i></p>
                        <textarea class="form-control" name="specialr"></textarea>
                  </div>
                  </div>
                   <div class="col-sm-12">
                   		<div class="checkbox">
                          <label>
                            <input type="checkbox" name="highfloor" value="highfloor">
                            I'd like a room on a higher floor (if available) 
                          </label>
                        </div>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="lowfloor" value="lowfloor">
                            I'd like a ground-level room (if available)
                          </label>
                        </div>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="parking" value="parking">
                           I'd like free parking
                          </label>
                        </div>
                        
                   </div>
                  </div>
          <button type="submit" class="btn btn-default" >Continue</button>
        </form>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		document.getElementById("selectroom").className="";
		document.getElementById("cfmr").className="";
		document.getElementById("rsvd").className="";
		document.getElementById("enterdetails").className="actived-selct";
	})
var d1pastval = new String();
var d2pastval = new String();
function validatemail(){
var d1val = document.getElementById(email1).value;
var d2val = document.getElementById(email2).value;
if(d1val.length&&d2val.length&&(d1val != d2val)&&((d1val != d1pastval)||(d2val != d2pastval) )) {

      alert("The 2 email fields must be identical.");
      d1pastval = d1val;
      d2pastval = d2val;
      return false;
   }
return true;
}
</script>

