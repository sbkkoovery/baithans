<div class="top-sec-load">
                <span class="top-bar" id="toggle-down"> </span>
                <span class="heading-1">Contact US</span>
            </div>
<div class="contact-us">

<form name="contactform" id="contactform" action="contactus.php" method="post">
	<div class="form-sec-contact booking-sec-fields">
    	<div class="form-group">
            <label>Name</label>
            <div class="input-group">
               <input type="text" name="name" id="name" class="form-control" required="required" />
               <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                </div>
      </div>
      <div class="form-group">
            <label>Email Adress</label>
            <div class="input-group">
               <input type="email" name="email" id="email" class="form-control" required="required" />
               <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                </div>
      </div>
      <div class="form-group">
            <label>Phone Number</label>
            <div class="input-group">
               <input type="text" name="phone" id="phone" class="form-control" required="required" />
               <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                </div>
      </div>
      <div class="form-group">
            <label>Message</label>
            <div class="form-text">
                <textarea name="message" id="message" required="required"></textarea>
            </div>
            </div>
            <div class="submit pull-right">
            	<button class="btn btn-default">Submit</button>
            </div>
            <div class="map-marker">
            	<img src="images/map-marker.png" />
            </div>
            <div class="map-viewer">
            	<a href="#" id="">View On Map</a>
            </div>
    </div>
    </form>
    
    
    <div class="clearfix"></div>
</div>
<script src="js/main.js" type="text/javascript"></script>