<div class="main-head-gallery">
	<p>CONTACT US</p>
</div>
<div class="container-chk">
	<div class="contact-us no-height">
    	<div class="contact-field booking-sec-fields">
        	<div class="row">
            	<div class="col-sm-6">
                	<div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Name</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" />
                                    <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Email Address</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" />
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Phone Number</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" />
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                     <label>Message</label>
                       <textarea class="form-control"></textarea>
                    </div>
                     <div class="submit pull-right">
                        <button class="btn btn-default">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>