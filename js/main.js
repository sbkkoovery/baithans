// JavaScript Document

$(document).ready(function(){
	
$("#load-page, .whole-category, .booking-details, #load-contents").perfectScrollbar();
	
	var toglecont = $("#mid-items");
	var lodpage = $("#load-page");

$("#menu-toggle").click(function(){
	$("#togglesidebar").addClass("toggle");
	$(".menu-right").addClass("menu-right-pull");
	return false;
});
$("#closebtn").click(function(){
	lodpage.removeClass("activewidth");
	var x = lodpage.width();
	toglecont.removeClass("active-width-left");
	$("#menu-right").fadeIn(200);
	$("#togglesidebar").removeClass("toggle");

	$(".menu-right").removeClass("menu-right-pull");
});
//$(".bookmark").click(function(){
//	$("#pop-bg-book").fadeIn(100);
//});
//$("#pop-bg-book").mouseup(function(e){
//        var subject = $(".container-popup"); 
//		if(e.target.id != subject.attr('id') && !subject.has(e.target).length) {
//             $("#pop-bg-book").fadeOut();
//        }
//});
$(".closebtns").click(function(){
	$(".booking-details").fadeOut(function(){
	$(".whole-category").fadeIn();
	});
});
$(".detail").click(function(){
	$(".whole-category").fadeOut(function(){
	$(this).parent(".read-book").parent(".col-sm-9").parent(".row").parent(".item-block").children(".booking-details").fadeIn();
	});
});
$("#navigationbar ul li").not(':first').click(function(){
	$("#menu-right").hide(50);
	toglecont.addClass("active-width-left");
	lodpage.addClass("activewidth");
	$("#bg-overlay").show();
	$("#wecome-msg-d").addClass("active-width-left");
	$(".video-pic").addClass("active-width-left");
	$("#closebtn").hide();
});
$("#map").click(function(){
	$("#map-sec").toggleClass("active-map");
	})
$(".top-bar").click(function(){
	lodpage.removeClass("activewidth");
	toglecont.removeClass("active-width-left");
	$("#map-sec").removeClass("active-map");
	$("#bg-overlay").hide();
	$("#wecome-msg-d").removeClass("active-width-left");
	$(".video-pic").removeClass("active-width-left");
	$("#menu-right").fadeIn();
	$("#closebtn").show();
});
	var miditemwidth = $("#mid-items");
	var innercontainer = $(".item-container");
	var itemsmain = $(".box-items");
	var itemsingle = $(".box-single");
	var itemswidth = itemsmain.width();
	var itemslist = miditemwidth.find(itemsingle);
	var itemlength =	itemslist.length;
	var totalwidth = itemlength *itemswidth;
	var slidecontainer =  $(".item-container");
	var windwidth = $(window).width();
	var first = $(".item-container .box-single:first-child");
	var last = $(".item-container .box-single:last-child");
	
	innercontainer.css({"width" : totalwidth});
	if(windwidth > 500){
		var headwid = $("#wecome-msg").width();
		$("#wecome-msg").css({"margin-left" : -headwid/2});
		
		$("#lang").click(function(){
			$("#language-menu").toggleClass("active-lang");
	});
	}
	if(windwidth > 1200){
	miditemwidth.css({"width" : totalwidth, "margin-left" : -totalwidth/2});
	}else if(windwidth > 400 && windwidth <1199){
		 miditemwidth.css({"width" : itemswidth*4, "overflow" : "hidden", "margin-left" : -(itemswidth*4)/2});
		 $('.item-container').owlCarousel({
				autoplay:true,
				autoplayTimeout:2000,
				loop:true,
				responsive:{
					600:{
						items:6
					},
					
				}
			});
	$("#menu-toggle").click(function(){
		$(".menu-right").toggleClass("menu-right-h");
	});
	}
	else if(windwidth < 400){
		
		$('.item-container').owlCarousel({
				autoplay:true,
				autoplayTimeout:2000,
				loop:true,
				responsive:{
					0:{
						items:6
					},
					600:{
						items:5
					},
					
				}
			});
		var mobmenu = $("#menu-right");
		var hodling = $("#holding-container");
		$("#mobile-menu").click(function(){
			hodling.fadeToggle(100);
				mobmenu.fadeToggle(100);
				return false;
			});
		$("#close-mob").click(function(){
			mobmenu.fadeOut(100);
			hodling.fadeIn(100);
			return false;
			});
		miditemwidth.css({"width" : itemswidth*2, "overflow" : "hidden", "margin-left" : -(itemswidth*2)/2});
	}
	 $(function() {
    $( ".datepicker" ).datepicker({
      showOn: "button",
      buttonImage: "images/calander.png",
      buttonImageOnly: true,
      buttonText: "Select date"
    });
  });
$(".box-items").mouseenter(function(){
	$("#video-pic").hide();
	$("#wecome-msg-d").hide();
	var vale = 	$(this).parent(".box-single").children("#takevalue").val();
	var smalval = $(this).parent(".box-single").children("#tagline").val();
	var logval = $(this).parent(".box-single").children("#logoval").attr('src');
	$(this).children(".rectangle-block").hide();
	$("#wecome-msg").show();
	$(".bolt-it").html(vale);
	$(".tagline").html(smalval);
	$("#hotel-logo").show();
	$(".hotel-logo img").attr("src", logval);
});
$(".box-items").mouseleave(function(){
	$(".rectangle-block").show();
});
$("#mid-items").mouseleave(function(){
	$(".rectangle-block").show();
	$("#video-pic").hide();
	$("#wecome-msg, #hotel-logo, #video-pic").hide();
	$("#wecome-msg-d").show();
});
$(".rectangles").mouseenter(function(){
	$("#wecome-msg-d, #wecome-msg, #hotel-logo").hide();
	var photo = $(this).children(".video-pic-1").children(".img-sec-change").children("img").attr('src');
	var photodesc = $(this).children().children(".changed-head").text();
	var smalldesc = $(this).children().children(".changed-desc").text();
	$(".rectangle-block").show();
	$(".img-sec img").attr("src", photo);
	$(".main-desc").html(photodesc);
	$(".second-desc").html(smalldesc);
	$("#video-pic").show();
});
$(".rectangles").mouseleave(function(){
	$("#video-pic").hide();
});
	
 var heightul = $(".nav-inside").height();
  var heigh = heightul/2;
$(".nav-inside").css({"margin-top" : -(heightul/2)});
	var winheight = $(window).height();
	var x = $("#main-content").css({"margin-top" : winheight});
	
	
});
 function loadcontent(dataload){
    $.ajax({
        type: "GET",
		url: dataload,
        data: { },
		beforeSend: function() {
              $("#loading-image").show();
           },
		
        success: function(data){
          $('#load-pages').html(data);
			 $("#loading-image").hide();
        }
    });
	
 }
 function loadstep(formload){
    $.ajax({
        type: "GET",
		url: formload,
        data: { },
		//beforeSend: function() {
//              $("#loading-image").show();
//           },
		
        success: function(data){
          $('#type-room').html(data);
			 //$("#loading-image").hide();
        }
    });
	
 }
	
	