// JavaScript Document
$(document).ready(function(){

	var winwidth = $(window).width();
	var winheight = $(window).height();
	var navwidth = $(".nav-bottm").width();
	//alert(navwidth);
	//$("#nav-bottm").css({"margin-left" : -(navwidth/2)});
	$("#nav-bottm li").click(function(){
		$("#about-us").addClass("bouncedown");
		$("#nav-bottm li").removeClass("actived");
		$("#nav-bottm li").addClass("hactived");
		
		$(this).addClass("actived");
	});
	$(".close-bounce").click(function(){
		$("#about-us").removeClass("bouncedown");
		$("#nav-bottm li").removeClass("actived");
		$("#nav-bottm li").removeClass("hactived");
		$("#nav-bottm").fadeIn(); 
	});
	$('.owl-carousel').owlCarousel({
	autoplay:true,
	autoplayHoverPause:true,
    stagePadding: 150,
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:3
        }
    }
})	;
$('.slider-inside').owlCarousel({
	autoplay:true,
	autoplayHoverPause:true,
	navText: [ '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>' ],
    loop:true,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})	;
	var owl = $(".owl-dots").width();
	$(".owl-dots").css({"margin-left" : -(owl/2)});
	 
	 $("#no-nav").click(function(){
		$("#nav-bottm").hide(); 
	});
	$(".nav-left ul li").not("#no-nav-inside").click(function(){
		$("#nav-bottm").fadeIn(); 
	});
	$("#slide-result").click(function(){
		$(".availablity-slide").slideDown();
	});
	$("#expand").click(function(){
		$("#map-sec").toggleClass("expand-map");
	})
});
 function loademe(loaddata){
    $.ajax({
        type: "GET",
		url: loaddata,
        data: { },
		beforeSend: function() {
              $("#inside").show();
			},
		
        success: function(data){
          $('#load_here').html(data);
		  $("#inside").hide();
        }
    });
	
 }